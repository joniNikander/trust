﻿using UnityEngine;
using System.Collections;

public class CameraShakes : MonoBehaviour {

    public Transform CameraTransform;
    public float ShakeDuration;
    public float ShakeAmount;
    public float DecreaseFactor;
    public bool shake = false;
    private float duration;
    private Vector3 OriginalPosition;
	// Use this for initialization
	void Start () {
        OriginalPosition = CameraTransform.position;
        duration = ShakeDuration;
	}
	
	// Update is called once per frame
	void Update () {
        if (shake)
        {
            if (ShakeDuration > 0)
            {
                CameraTransform.position = CameraTransform.position + Random.insideUnitSphere * ShakeAmount;
                ShakeDuration -= DecreaseFactor * Time.deltaTime;
            }
            if (ShakeDuration <= 0)
            {
                shake = false;
                Vector3 pos = CameraTransform.position;
                ShakeDuration = 0;
                CameraTransform.position = Vector3.MoveTowards(pos, OriginalPosition, 2 * Time.deltaTime);

            }
        }
        else
        {
            OriginalPosition = CameraTransform.position;
            ShakeDuration = duration;
        }
	}

    void OnAwake()
    {
        if(CameraTransform == null)
        {
            CameraTransform = GetComponent(typeof(Transform)) as Transform;
        }
    }
    void OnEnable()
    {
        OriginalPosition = CameraTransform.position;
    }
    public void SetShake(bool val)
    {
        shake = val;
    }

}
