﻿using UnityEngine;
using System.Collections;

public class CutsceneMove : MonoBehaviour {
    public GameObject Target;
    public GameObject MainCamera;
    public GameObject CutsceneCamera;
    public float CameraSpeed;
    public float RotationSpeed;
    public float Magnitude;
    public bool rotate;
    public bool StartCutScene;
    private Vector3 targetPositionMove;
	// Use this for initialization
	void Start () {        
	}
	
	// Update is called once per frame
	void Update () {
        if (StartCutScene)
            StartCutscene();
	
	}
    void StartCutscene()
    {
        MainCamera.SetActive(false);
        CutsceneCamera.SetActive(true);

        if(targetPositionMove != Vector3.zero)
        {
            if (CutsceneCamera.transform.position != targetPositionMove)
            {
                float step = CameraSpeed * Time.deltaTime;
                CutsceneCamera.transform.position = Vector3.MoveTowards(transform.position, targetPositionMove, step * Time.deltaTime);
            }
            if (CutsceneCamera.transform.position == targetPositionMove)
            {
                Vector3 rotatePos = targetPositionMove;
                Vector3 newDir;
                if (rotate)
                {
                    newDir = Vector3.RotateTowards(transform.forward, rotatePos, RotationSpeed * Time.deltaTime, Magnitude);
                    transform.rotation = Quaternion.LookRotation(newDir);
                }
            }
        }
        else
        {
            if (CutsceneCamera.transform.position != Target.transform.position)
            {
                float step = CameraSpeed * Time.deltaTime;
                CutsceneCamera.transform.position = Vector3.MoveTowards(transform.position, Target.transform.position, step * Time.deltaTime);
            }
            if (CutsceneCamera.transform.position == Target.transform.position)
            {
                Vector3 rotatePos = Target.transform.position;
                Vector3 newDir;
                if (rotate)
                {
                    newDir = Vector3.RotateTowards(transform.right, rotatePos, RotationSpeed * Time.deltaTime, Magnitude);
                    transform.rotation = Quaternion.LookRotation(newDir);
                }
                else {  
                    MainCamera.SetActive(true);
                    CutsceneCamera.SetActive(false);
                }
            }
        }
        
    }
    public void SetCutscene(Vector3 pos)
    {
        targetPositionMove = pos;
    }
}
