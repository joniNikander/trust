﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class camerafader : MonoBehaviour {
    public float fadeSpeed = 1.5f;          // Speed that the screen fades to and from black.
    public GameObject target;
    public GameObject follow;

    private float maxvalue;
    private float minvalue = 0;
    private float speed;
    private float oldspeed;
    private float currentspeed;
                                            // Use this for initialization
    void Start () {
        //target = GameObject.FindGameObjectWithTag("Player_One");
        GetComponent<SpriteRenderer>().color = Color.clear;
        minvalue = target.GetComponent<Fear>().FearMinimumValue;
    }

    // Update is called once per frame
    void Update()
    {
        maxvalue = target.GetComponent<Fear>().GetCurrentFear();
        speed = (maxvalue - minvalue) / (target.GetComponent<Fear>().FearMaximumValue - minvalue);
        fadeSpeed = speed;

        if (!target.GetComponent<movement_player_one>().IsHugging())
            EndScene();
        else if (target.GetComponent<movement_player_one>().IsHugging())
            StartScene();
    }
    void Awake()
    {
        // Set the texture so that it is the the size of the screen and covers it.
       // GetComponent<SpriteRenderer>().pixelInset = new Rect(0, 0, Screen.width, Screen.height);

    }
    void FadeToClear()
    {
        // Lerp the colour of the texture between itself and transparent.
        if(GetComponent<SpriteRenderer>().color != Color.clear)
            GetComponent<SpriteRenderer>().color = Color.Lerp(GetComponent<SpriteRenderer>().color, Color.clear, 0.66f * Time.deltaTime);

    }


    void FadeToBlack()
    {
        // Lerp the colour of the texture between itself and black.

        Color old = GetComponent<SpriteRenderer>().color;
        GetComponent<SpriteRenderer>().color = Color.Lerp(old, Color.white, fadeSpeed * Time.deltaTime);
        // GetComponent<GUITexture>().color = Color.Lerp(GetComponent<GUITexture>().color, f, fadeSpeed * Time.deltaTime);
    }


    void StartScene()
    {
        // Fade the texture to clear.
        FadeToClear();

        // If the texture is almost clear...
        if (GetComponent<SpriteRenderer>().color.a <= 0.05f)
        {
            // ... set the colour to clear and disable the GUITexture.
            GetComponent<SpriteRenderer>().color = Color.clear;
            GetComponent<SpriteRenderer>().enabled = false;
        }
    }


    public void EndScene()
    {
        float timer = 5f;
        // Make sure the texture is enabled.
        GetComponent<SpriteRenderer>().enabled = true;

        // Start fading towards black.
        FadeToBlack();

        // If the screen is almost black...
        if (GetComponent<SpriteRenderer>().color.a >= 0.99f)
            // ... reload the level.
        timer -= Time.deltaTime;
        if(timer <= 0f)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            timer = 0f;
        }
    }
}