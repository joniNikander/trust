using System;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class Camera2DFollow : MonoBehaviour
    {
        public Transform target;
        public float damping = 1;
        public float lookAheadFactor = 3;
        public float lookAheadReturnSpeed = 0.5f;
        public float lookAheadMoveThreshold = 0.1f;
        public float CameraYPosition;

        private float m_OffsetZ;
        private bool isZooming = false;
        private bool isReturning = false;
        private Vector3 m_LastTargetPosition;
        private Vector3 m_CurrentVelocity;
        private Vector3 m_LookAheadPos;
        private Vector3 CameraYPositionFactor;
        private Vector3 originalPosition;
        private Vector3 targetZoomPosition;
        // Use this for initialization
        private void Start()
        {
            m_LastTargetPosition = target.position;
            m_OffsetZ = (transform.position - target.position).z;
            transform.parent = null;
            CameraYPositionFactor = new Vector3(0.0f, CameraYPosition, 0.0f);
        }


        // Update is called once per frame
        private void Update()
        {
            if (!isZooming)
            {
                originalPosition = transform.position;
                SetZoomPosition(target.transform.position);

                // only update lookahead pos if accelerating or changed direction
                float xMoveDelta = (target.position - m_LastTargetPosition).x;

                bool updateLookAheadTarget = Mathf.Abs(xMoveDelta) > lookAheadMoveThreshold;

                if (updateLookAheadTarget)
                {
                    m_LookAheadPos = lookAheadFactor * Vector3.right * Mathf.Sign(xMoveDelta);
                }
                else
                {
                    m_LookAheadPos = Vector3.MoveTowards(m_LookAheadPos, CameraYPositionFactor, Time.deltaTime * lookAheadReturnSpeed);
                }

                Vector3 aheadTargetPos = target.position + m_LookAheadPos + Vector3.forward * m_OffsetZ;
                Vector3 newPos = Vector3.SmoothDamp(transform.position, aheadTargetPos, ref m_CurrentVelocity, damping);

                transform.position = newPos;

                m_LastTargetPosition = target.position;
            }else if(isZooming)
            {
                Zoom();
            }

        }
        private void Zoom()
        {
            if(isZooming)
            {
                transform.position = Vector3.Slerp(transform.position, targetZoomPosition, 3f * Time.deltaTime);
                if(transform.position.Equals(targetZoomPosition))
                {
                    isZooming = false;
                }
            }
            if(!isZooming && isReturning)
            {
                transform.position = Vector3.Slerp(transform.position, originalPosition, 3f * Time.deltaTime);
                if (transform.position.Equals(originalPosition))
                {
                    isReturning = false;
                }
            }
        }
        private void SetZoomPosition(Vector3 val)
        {
            float z = val.z / 2;
            float y = val.y * 1.5f;
            targetZoomPosition = new Vector3(val.x, y, z);
        }
    }
}
