﻿using UnityEngine;
using System.Collections;



public class TwigSound : MonoBehaviour {
    public AudioClip twig;
    AudioSource Sticks;

	// Use this for initialization
	void Start () {
        Sticks = this.gameObject.GetComponent<AudioSource>();

    }

  

	void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.tag ==  ("Player_One"))   
        {
            Debug.Log("Hello");

            Sticks.PlayOneShot(twig, 1);
        }
    }
	// Update is called once per frame
	void Update () {

	}
}

	