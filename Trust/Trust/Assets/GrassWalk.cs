﻿using UnityEngine;
using System.Collections;

public class GrassWalk : MonoBehaviour {

    public AudioClip Grass;
    AudioSource grassy;

	// Use this for initialization
	void Start () {
        grassy = this.gameObject.GetComponent<AudioSource>();
    }
//coll.gameObject.tag == ("Player_One") && 
    void OnTriggerEnter(Collider coll)
    {
      
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.LeftArrow))
        {
            Debug.Log("Hello");

            grassy.Play();
        }

        else if (Input.GetKeyUp(KeyCode.RightArrow) || Input.GetKeyUp(KeyCode.LeftArrow))
        {
            Debug.Log("Hello Again");

            grassy.Stop();
        }

    }
}
