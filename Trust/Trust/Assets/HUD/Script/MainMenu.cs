﻿using UnityEngine;
using System.Collections;
using System;
public class MainMenu : MonoBehaviour
{
    public bool DebugControls;
    public Vector3 target;
    private Camera cam;
    public GameObject player;
    CanvasRenderer o;
    bool movecam = false;
    void Start()
    {

        player.GetComponent<Animator>().SetBool("idle", true);
        player.GetComponent<Animator>().SetBool("girlidle", false);
        cam = Camera.main;
  
       // this.GetComponentInChildren<AudioSource>().enabled = true;
        movecam = false;
    }
    public string startLevel;
    void Update()
    {
        NewGame();
        if(movecam)
        {           
            MoveCamera();
        }

    }
    public void NewGame()
    {
        if (DebugControls)
        {
            if (Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.LeftArrow) && Input.GetKey(KeyCode.RightArrow))
            {
                //UnityEngine.SceneManagement.SceneManager.LoadScene(startLevel);
                movecam = true;
                GameObject.FindGameObjectWithTag("Respawn").GetComponent<test>().setTrue(true);

            }
        }
        else if(!DebugControls)
        {
            if (Input.GetKey(KeyCode.Joystick2Button3) && Input.GetKey(KeyCode.Joystick2Button1) && Input.GetKey(KeyCode.Joystick1Button3) && Input.GetKey(KeyCode.Joystick1Button1))
            {
                //UnityEngine.SceneManagement.SceneManager.LoadScene(startLevel);
                movecam = true;
                GameObject.FindGameObjectWithTag("Respawn").GetComponent<test>().setTrue(true);

            }
        }    
           
    }
    private void MoveCamera()
    {
        if (cam.transform.position == target)
            cam.transform.position = Vector3.Lerp(cam.transform.position, new Vector3(0f, 0f, 10f), 1f * Time.deltaTime);
        player.GetComponent<Animator>().SetInteger("direction", 12);
        player.GetComponent<Animator>().SetBool("isboyhugging", true);
        player.GetComponent<Animator>().SetBool("idle", false);
       // player.transform.position = Vector3.Lerp(player.transform.position, new Vector3(0f, 0f, 25f), 0.33f * Time.deltaTime);
        cam.transform.position = Vector3.Lerp(cam.transform.position, target, 1f * Time.deltaTime);
    }
}
