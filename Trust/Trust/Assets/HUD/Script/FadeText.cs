﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class FadeText : MonoBehaviour {
    public float timerForFadeIn;
    public float timerForFadeOut;
    public float[] fadein;
    public float[] fadeout;
    public float[] fadespeed;
    private Color toLerpSolid;
    private Color toLerpTransparent;
    private CanvasRenderer[] targetRenderer;
    public bool[] started;
    public bool[] startfadein;
    public bool[] startfadeout;
    public int numberOfArrays;
    public int whichBool;
    // Use this for initialization
    void Start()
    {
        whichBool = 0;
        toLerpSolid = GetComponentInChildren<CanvasRenderer>().GetColor();
        toLerpSolid.a = 1f;

        toLerpTransparent = GetComponentInChildren<CanvasRenderer>().GetColor();
        toLerpTransparent.a = 0f;

        numberOfArrays = GetComponentsInChildren<CanvasRenderer>().Length;
        targetRenderer = GetComponentsInChildren<CanvasRenderer>();

        startfadein = new bool[numberOfArrays];
        startfadeout = new bool[numberOfArrays];
        started = new bool[numberOfArrays];

        fadein = new float[numberOfArrays];
        fadeout = new float[numberOfArrays];
        fadespeed = new float[numberOfArrays];

        for (int i = 0; i < numberOfArrays; i++)
        {
            startfadein[i] = false;
            startfadeout[i] = false;
            started[i] = false;
            fadespeed[i] = 3f;
            fadein[i] = timerForFadeIn;
            fadeout[i] = timerForFadeOut;
   
        }
        for (int i = whichBool + 2; i < numberOfArrays; i++)
        {
            targetRenderer[i].GetComponent<CanvasRenderer>().gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Joystick1Button0) || Input.GetKeyDown(KeyCode.Joystick1Button1) || Input.GetKeyDown(KeyCode.Joystick2Button0) || Input.GetKeyDown(KeyCode.Joystick2Button1))
        {
            SceneManager.LoadScene("Menu_Start");
        }
        if (!started[whichBool])
        {
            targetRenderer[whichBool].GetMaterial().color = Color.clear;
        }

        fadein[whichBool] -= Time.deltaTime;

        if (fadein[whichBool] <= 0f)
        {
            started[whichBool] = true;
            if (!startfadeout[whichBool])
                startfadein[whichBool] = true;
        }
        if (startfadein[whichBool] && !startfadeout[whichBool] && started[whichBool])
            Fadein(whichBool);
        if (targetRenderer[whichBool].GetMaterial().color.a >= 0.99f)
        {
            fadeout[whichBool] -= Time.deltaTime;
            startfadein[whichBool] = false;
        }
        if (fadeout[whichBool] <= 0f && !startfadein[whichBool])
        {
            startfadeout[whichBool] = true;
        }
        if (startfadeout[whichBool] && !startfadein[whichBool] && started[whichBool])
            Fadeout(whichBool);
        if (targetRenderer[whichBool].GetMaterial().color.a <= 0.01f && startfadeout[whichBool])
        {
            startfadein[whichBool] = false;
            startfadeout[whichBool] = false;
            fadeout[whichBool] = 0f;
            fadein[whichBool] = 0f;
            whichBool++;
            if (whichBool == 1)
                whichBool++;
            if (whichBool <= numberOfArrays)
            { 
                targetRenderer[whichBool].GetComponent<CanvasRenderer>().gameObject.SetActive(true);
                targetRenderer[whichBool - 1].GetComponent<CanvasRenderer>().gameObject.SetActive(false);
                if (targetRenderer[whichBool - 2])
                    targetRenderer[whichBool - 2].GetComponent<CanvasRenderer>().gameObject.SetActive(false);
            }
        }
    }
    private void Fadein(int val)
    {   
        //GUI.contentColor = Color.Lerp(GUI.contentColor, Color.white, fadespeed * Time.deltaTime);
        targetRenderer[val].GetMaterial().color = Color.Lerp(targetRenderer[val].GetMaterial().color, toLerpSolid, fadespeed[val] * Time.deltaTime);
    }

    private void Fadeout(int val)
    {
        //GUI.color = Color.Lerp(GUI.color, Color.clear, fadespeed * Time.deltaTime);
        targetRenderer[val].GetMaterial().color = Color.Lerp(targetRenderer[val].GetMaterial().color, toLerpTransparent, fadespeed[val] * Time.deltaTime);
    }
}
