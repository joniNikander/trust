﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets;
public class endGame : MonoBehaviour {
    private bool start = false;
    private GameObject target;
    private GameObject blackFade;
    private Color color;
    float timer = 2.5f;
    // Use this for initialization
    void Start () {
        blackFade = GameObject.FindGameObjectWithTag("Plank");
        target = GameObject.FindGameObjectWithTag("destroy");
        color = GameObject.FindGameObjectWithTag("destroy").GetComponent<SpriteRenderer>().material.color;
        color.a = 0f;
    }
	
	// Update is called once per frame
	void Update () {
        if (!start)
        {
            blackFade.GetComponent<SpriteRenderer>().color = Color.clear;
            target.GetComponent<SpriteRenderer>().material.color = color;
        }

        if (start)
        {
            Fader(target);
            if (target.GetComponent<SpriteRenderer>().material.color.a >= 0.99f)
            {
                timer -= Time.deltaTime;
                if(timer <= 0f)
                    UnityEngine.SceneManagement.SceneManager.LoadScene("Credits");               
            }

        }
	
	}
    private void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.name == "loader")
        {
            start = true;
            GetComponent<movement_player_one>().enabled = false;
        }
    }
    private void Fader(GameObject obj)
    {
        blackFade.GetComponent<SpriteRenderer>().color = Color.Lerp(blackFade.GetComponent<SpriteRenderer>().color, Color.black, 2.5f * Time.deltaTime);
        if(blackFade.GetComponent<SpriteRenderer>().color.a >= 0.99f)
        {
            if (color.a <= 1f)
                color.a += 0.4f * Time.deltaTime;
            obj.GetComponent<SpriteRenderer>().material.color = color;
        }
    }
}
