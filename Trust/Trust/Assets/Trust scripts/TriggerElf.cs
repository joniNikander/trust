﻿using UnityEngine;
using System.Collections;

public class TriggerElf : MonoBehaviour {

    private GameObject Faerie;

	// Use this for initialization
	void Start () {

        Faerie = GameObject.FindGameObjectWithTag("Oldtroll");
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.tag == "Player_One")
        {
            Faerie.GetComponent<TestCorridorElf>().SetMovable(true);
        }
    }
}
