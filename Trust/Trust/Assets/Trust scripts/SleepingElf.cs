﻿using UnityEngine;
using System.Collections;

public class SleepingElf : MonoBehaviour
{
    public bool Moveable;
    public Transform Target;
    public Transform Target1;
    public Transform Target2;
    public Transform Target3;
    public Transform Target4;
    public AudioClip twigelf;
    AudioSource Audio;

    public Animator Waking;

    private Color Scared;
    private Color Normal;


    private bool Stone;
    private bool Stone1;
    private bool Stone2;
    private bool Stone3;
    private bool Stone4;

    private Color newcolor;
    private Renderer elf;
    public float Speed;
    public float Timer;
    private float Anim_Timer;
    private bool Timer_Start;


    private Animator Elf;


    // Use this for initialization
    void Start()
    {
        GameObject.FindGameObjectWithTag("Player_One").GetComponent<StickCollision>().enabled = false;
        elf = this.GetComponent<Renderer>();
        newcolor = elf.material.color;
        Timer_Start = false;

        Elf = this.gameObject.GetComponent<Animator>();
        Waking = Waking.GetComponent<Animator>();
        
        Elf.enabled = true;
        Moveable = true;
        Stone = true;
        Stone1 = false;
        Stone2 = false;
        Stone3 = false;
        Stone4 = false;

        Scared = new Vector4(255, 0, 0, 255);

        Normal = new Vector4(255, 255, 255, 255);

        Audio = this.GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update()
    {
        float step = Speed * Time.deltaTime;


        Anim_Timer += Time.deltaTime;
        if(Anim_Timer >= 1.36f)
        {
            Elf.speed = 0;
        }

        if (Timer_Start == true)
        {
            Timer -= Time.deltaTime;

            if (Timer <= 0)
            {
                Moveable = true;
            }
        }

        if (Moveable == false)
        {
            this.GetComponent<SpriteRenderer>().color = Scared;
        }
        else
            this.GetComponent<SpriteRenderer>().color = Normal;



        if (Moveable == true)
        {
            if (Stone == true)
            {
                Vector3 Target1 = new Vector3(Target.position.x, transform.position.y, Target.position.z);

                transform.position = Vector3.MoveTowards(transform.position, Target1, step);
            }
            if (Stone1 == true && Stone == false)
            {
                Vector3 Target2 = new Vector3(Target1.position.x, transform.position.y, Target1.position.z);
                transform.position = Vector3.MoveTowards(transform.position, Target2, step);
            }
            if (Stone2 == true && Stone1 == false)
            {
                Vector3 Target3 = new Vector3(Target2.position.x, transform.position.y, Target2.position.z);
                transform.position = Vector3.MoveTowards(transform.position, Target3, step);
            }
            if (Stone3 == true && Stone2 == false)
            {
                Vector3 Target4 = new Vector3(Target3.position.x, transform.position.y, Target3.position.z);
                transform.position = Vector3.MoveTowards(transform.position, Target4, step);

            }
            if (Stone4 == true && Stone3 == false)
            {
                Vector3 Target5 = new Vector3(Target4.position.x, transform.position.y, Target4.position.z);
                transform.position = Vector3.MoveTowards(transform.position, Target5, step);
            }

            if(Stone4 == false)
            {
                
            }

        }




    }
    void OnTriggerEnter(Collider col)
    {

        if (col.gameObject.name == "Trigger1")
        {
           // GameObject.FindGameObjectWithTag("Player_One").GetComponent<StickCollision>().enabled = false;
            GameObject.FindGameObjectWithTag("Player_One").GetComponent<movement_player_one>().enabled = false;

            Stone1 = true;
            Stone = false;
            Moveable = false;
            Timer_Start = true;
         
            Speed = 15;
            Waking.SetBool("WakingUp", true);

            Audio.PlayOneShot(twigelf, 1);




        }
        if (col.gameObject.name == "Trigger2")
        {
            Stone2 = true;
            Stone1 = false;
            Waking.SetBool("WakingUp", false);


        }
        if (col.gameObject.name == "Trigger3")
        {
            Stone3 = true;
            Stone2 = false;
        




        }
        if (col.gameObject.name == "Trigger4")
        {
            Stone4 = true;
            Stone3 = false;


        }
        if (col.gameObject.name == "Trigger5")
        {
            // Stone5 = true;
            Stone4 = false;
            GameObject.FindGameObjectWithTag("Player_One").GetComponent<StickCollision>().enabled = true;
            GameObject.FindGameObjectWithTag("Player_One").GetComponent<movement_player_one>().enabled = true;
        }
 
    }

    void OnTriggerStay(Collider col)
    {
       if(col.name == "Trigger5")
            {
                newcolor.a -= 1f * Time.deltaTime;
                elf.material.color = newcolor;
            }
        if (elf.material.color.a <= 0)
        {
            GetComponent<SleepingElf>().enabled = false;
            elf.enabled = false;
        }

    }

    /*   if (col.gameObject.name == "StoneSix")
       {
           Stone5 = false;
           Goal = true;

       }
       if (col.gameObject.name == "EventFinish")
       {
           Path = true;
           Goal = false;




       }
   }*/
    public void SetMovable(bool p_bValue)
    {
        Moveable = p_bValue;
    }


}
