﻿using UnityEngine;
using System.Collections;

public class TreeArms : MonoBehaviour {

    private GameObject ArmLeft;
    private GameObject ArmRight;
    private GameObject ArmAbove;

    private Vector3 Orignial_Position1;
    private Vector3 Orignial_Position2;
    private Vector3 Orignial_Position3;

    public Transform Target;

    public bool Active;

    public float Speed;
    

    // Use this for initialization
    void Start () {

        Active = false;

        ArmLeft = GameObject.FindGameObjectWithTag("ArmLeft");
        ArmRight = GameObject.FindGameObjectWithTag("ArmRight");
        ArmAbove = GameObject.FindGameObjectWithTag("ArmAbove");

        ArmLeft.GetComponent<Renderer>().enabled = false;
        ArmRight.GetComponent<Renderer>().enabled = false;
        ArmAbove.GetComponent<Renderer>().enabled = false;

        Orignial_Position1 = ArmLeft.transform.position;
        Orignial_Position2 = ArmRight.transform.position;
        Orignial_Position3 = ArmAbove.transform.position;

        ArmAbove.SetActive(false);
        ArmLeft.SetActive(false);
        ArmRight.SetActive(false);

    

    }
	
	// Update is called once per frame
	void Update () {

        float step = Speed * Time.deltaTime;

        if (Active == true)
        {
            ArmAbove.transform.position = Vector3.MoveTowards(ArmAbove.transform.position, Target.position, step);
            ArmRight.transform.position = Vector3.MoveTowards(ArmRight.transform.position, Target.position, step);
            ArmLeft.transform.position = Vector3.MoveTowards(ArmLeft.transform.position, Target.position, step);

            this.GetComponent<Fear>().SetCurrentFear(20f);
        }
        if (this.GetComponent<movement_player_one>().IsHugging() == true)
        {

            Active = false;

        }
        if (Active == false)
        {
            ArmAbove.transform.position = Vector3.MoveTowards(ArmAbove.transform.position, Orignial_Position3, step);
            ArmRight.transform.position = Vector3.MoveTowards(ArmRight.transform.position, Orignial_Position2, step);
            ArmLeft.transform.position = Vector3.MoveTowards(ArmLeft.transform.position, Orignial_Position1, step);
        }



    }

    void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.name == "ArmTrigger")
        {
            ArmLeft.GetComponent<Renderer>().enabled = true;
            ArmRight.GetComponent<Renderer>().enabled = true;
            ArmAbove.GetComponent<Renderer>().enabled = true;

            ArmAbove.SetActive(true);
            ArmLeft.SetActive(true);
            ArmRight.SetActive(true);

         

            Active = true;

        }
    }
}
