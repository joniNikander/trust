﻿using UnityEngine;
using System.Collections;

public class stoneFeedback : MonoBehaviour {
    private GameObject ferieObject;
    private GameObject stoneObject;
    private Color originalColor;
    private Color fadeColor;
    private bool startFade;
    private bool fadebackcolor;
    private bool startTimer;
    private float B = 3f;

	// Use this for initialization
	void Start () {

        stoneObject = this.gameObject;
        originalColor = stoneObject.GetComponent<SpriteRenderer>().color;
        fadebackcolor = false;
        fadeColor = Color.yellow;
        startTimer = false;
    }
	
	// Update is called once per frame
	void Update () {
            Flash();
	}
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Faerie")
        {
            startFade = true;
            startTimer = true;
            
            fadebackcolor = false;
        }
    }
    void Flash()
    {
        if (startFade)
        {
            stoneObject.GetComponent<SpriteRenderer>().color = Color.Lerp(fadeColor, originalColor, 2.0f * Time.deltaTime);
            if (timer())
            {
                fadebackcolor = true;
                startFade = false;
            }
        }
        if (fadebackcolor)
        {
            stoneObject.GetComponent<SpriteRenderer>().color = Color.Lerp(originalColor, fadeColor, 2.0f * Time.deltaTime);
            startTimer = false;
        }
       
    }

    bool timer()
    {
        if(startTimer)
        {
            B -= Time.deltaTime;
            if (B < 0f)
                return true;
        }          
        return false;
    }
}
