﻿using UnityEngine;
using System.Collections;

public class SetPlayerMoveable : MonoBehaviour {

    private GameObject Player;


	// Use this for initialization
	void Start () {

        Player = this.gameObject;
	
	}
	
	// Update is called once per frame
	void Update () {

	
	}

    void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.name == "EventStart")
        Player.GetComponent<movement_player_one>().SetMoveable(false);
    }
}
