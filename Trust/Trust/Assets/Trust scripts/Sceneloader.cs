﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Sceneloader : MonoBehaviour {
    public string SceneToLoad;
    public int[] SceneList;
    public int Scene;
    private bool[] Completed;
    private bool[] CheckForCompletion;

    void Start()
    {
        SceneList = new int[SceneManager.sceneCountInBuildSettings];
        Completed = new bool[SceneManager.sceneCountInBuildSettings];
        CheckForCompletion = new bool[SceneManager.sceneCountInBuildSettings];
        for (int i = 0; i < SceneManager.sceneCountInBuildSettings; i++)
        {
            SceneList[i] = i;
            Completed[i] = false;
            CheckForCompletion[i] = true;
        }      
    }

    void Update()
    {
       

    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player_One")
        {
            if(CheckScene(SceneToLoad))
                SceneManager.LoadScene(SceneToLoad);
            if (AllScenesCompleted())
            {
                SceneManager.LoadScene("EndScene");
            }
        }
    }
    private bool CheckScene(string name)
    {
        if (name == SceneManager.GetActiveScene().name)
            return false;
        else
            return true;
    }
    bool AllScenesCompleted()
    {
        if (Completed.Equals(CheckForCompletion))
            return true;
        else
            return false;
    }
    void SetCompletionForScene(bool value, int scene)
    {
        Completed[scene] = value;
    }
}
