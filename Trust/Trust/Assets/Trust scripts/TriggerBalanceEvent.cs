﻿using UnityEngine;
using System.Collections;

public class TriggerBalanceEvent : MonoBehaviour
{
    public GameObject target;
    // Use this for initialization
    void Start()
    {
        //target = GameObject.FindGameObjectWithTag("Player_One");	
    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player_One")
            target.GetComponent<BalancingScript>().SetEvents(true);
    }
}
