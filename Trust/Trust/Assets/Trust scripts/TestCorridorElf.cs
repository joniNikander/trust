﻿using UnityEngine;
using System.Collections;

public class TestCorridorElf : MonoBehaviour {

    public bool Moveable;
    public Transform Target;
    public Transform Target1;
    public Transform Target2;
    public Transform Target3;
    public Transform Target4;
   /* public Transform Target5;
    public Transform Finish;
    public Transform Way;*/

    private GameObject Troll;

    private bool Stone;
    private bool Stone1;
    private bool Stone2;
    private bool Stone3;
    private bool Stone4;
   /* private bool Stone5;
    private bool Stone6;
    private bool Goal;
    public bool Path;*/

    public float Speed;
    public float Timer;
    //public float Timer;

    private int Puzzle;
    private Animator Elf;

    private Color newcolor;
    private Renderer elf;
    // Use this for initialization
    void Start()
    {

        elf = this.GetComponent<Renderer>();
        newcolor = elf.material.color;

        Elf = this.gameObject.GetComponent<Animator>();

        Elf.enabled = true;
        Moveable = true;
        Stone = true;
        Stone1 = false;
        Stone2 = false;
        Stone3 = false;
        Stone4 = false;
       /* Stone5 = false;
        Path = false;*/




    }

    // Update is called once per frame
    void Update()
    {
        float step = Speed * Time.deltaTime;

        //Timer -= Time.deltaTime;


        Timer += Time.deltaTime;

        if (Timer >= 1.36f)
        {
            Elf.speed = 0;
        }


        
        if (Moveable == true)
        {
            if (Stone == true)
            {
                Vector3 Target1 = new Vector3(Target.position.x, transform.position.y, Target.position.z);

                transform.position = Vector3.MoveTowards(transform.position, Target1, step);
            }
            if (Stone1 == true && Stone == false)
            {
                Vector3 Target2 = new Vector3(Target1.position.x, transform.position.y, Target1.position.z);
                transform.position = Vector3.MoveTowards(transform.position, Target2, step);
            }
            if (Stone2 == true && Stone1 == false)
            {
                Vector3 Target3 = new Vector3(Target2.position.x, transform.position.y, Target2.position.z);
                transform.position = Vector3.MoveTowards(transform.position, Target3, step);
            }
            if (Stone3 == true && Stone2 == false)
            {
                Vector3 Target4 = new Vector3(Target3.position.x, transform.position.y, Target3.position.z);
                transform.position = Vector3.MoveTowards(transform.position, Target4, step);
         
            }
            if (Stone4 == true && Stone3 == false)
            {
                Vector3 Target5 = new Vector3(Target4.position.x, transform.position.y, Target4.position.z);
                transform.position = Vector3.MoveTowards(transform.position, Target5, step);
            }
           /* if (Stone5 == true && Stone4 == false)
            {
                transform.position = Vector3.MoveTowards(transform.position, Target5.position, step);
            }
            if (Goal == true && Stone5 == false)
            {
                transform.position = Vector3.MoveTowards(transform.position, Way.position, step);
            }
            if (Goal == false && Path == true)
            {
                transform.position = Vector3.MoveTowards(transform.position, Way.position, step);
            }*/
        }




    }
    void OnTriggerEnter(Collider col)
    {

        if (col.gameObject.name == "Trigger1")
        {
            Stone1 = true;
            Stone = false;
            Moveable = false;
        



        }
        if (col.gameObject.name == "Trigger2")
        {
            Stone2 = true;
            Stone1 = false;
            Moveable = false;

        }
        if (col.gameObject.name == "Trigger3")
        {
            Stone3 = true;
            Stone2 = false;
            Moveable = false;



        }
        if (col.gameObject.name == "Trigger4")
        {
            Stone4 = true;
            Stone3 = false;
            Moveable = false;

        }
        if (col.gameObject.name == "Trigger5")
        {
            // Stone5 = true;
            Stone4 = false;
  
        }
    }
    
    void OnTriggerStay(Collider col)
    {
        if(col.gameObject.name == "Trigger5")
        {
            newcolor.a -= 1f * Time.deltaTime;
            elf.material.color = newcolor;
            if (elf.material.color.a <= 0)
            {
                GetComponent<TestCorridorElf>().enabled = false;
                elf.enabled = false;
            }

        }
    }

        

     /*   if (col.gameObject.name == "StoneSix")
        {
            Stone5 = false;
            Goal = true;

        }
        if (col.gameObject.name == "EventFinish")
        {
            Path = true;
            Goal = false;
         



        }
    }*/
    public void SetMovable(bool p_bValue)
    {
        Moveable = p_bValue;
    }


}
