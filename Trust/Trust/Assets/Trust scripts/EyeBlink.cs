﻿using UnityEngine;
using System.Collections;

public class EyeBlink : MonoBehaviour {

    private GameObject Blink;
    public float Duration;
    private float OldDuration;

	// Use this for initialization
	void Start () {
        OldDuration = Duration;
        Blink = this.gameObject;
        Blink.GetComponent<ParticleSystem>().Play();

    }
	
	// Update is called once per frame
	void Update () {

        Duration -= Time.deltaTime;
        
        if (Duration <= 0)
        {
            Blink.GetComponent<ParticleSystem>().Stop();
            
        }
        if(Duration <= -4)
        {
            Duration = OldDuration;
            Blink.GetComponent<ParticleSystem>().Play();
        }
        
	}
}
