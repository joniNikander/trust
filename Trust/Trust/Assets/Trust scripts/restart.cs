﻿using UnityEngine;
using System.Collections;

public class restart : MonoBehaviour {
    float timer = 3f;
	// Use this for initialization
	void Start () {
        GameObject.FindGameObjectWithTag("destroy").GetComponent<SpriteRenderer>().color = Color.clear;
	
	}
	
	// Update is called once per frame
	void Update () {
	    if(this.gameObject.activeSelf)
        {
            timer -= Time.deltaTime;
            if (timer <= 0f)
            {
                GameObject.FindGameObjectWithTag("destroy").GetComponent<SpriteRenderer>().color = Color.Lerp(GameObject.FindGameObjectWithTag("destroy").GetComponent<SpriteRenderer>().color, Color.black, 1f * Time.deltaTime);
                if (GameObject.FindGameObjectWithTag("destroy").GetComponent<SpriteRenderer>().color.a >= 0.99f)
                    UnityEngine.SceneManagement.SceneManager.LoadScene("Menu_Start");
            }

        }
	}
}
