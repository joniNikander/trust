﻿using UnityEngine;
using System.Collections;

public class alphavalue : MonoBehaviour {
    public CanvasRenderer[] targetRenderer;
    public float[] alpha;
    bool start;
    private Color toLerpSolid;
    // Use this for initialization
    void Start () {
        start = true;
        targetRenderer = new CanvasRenderer[3];
        alpha = new float[3];
        toLerpSolid = GetComponentInChildren<CanvasRenderer>().GetColor();
        toLerpSolid.a = 1f;
        targetRenderer = GetComponentsInChildren<CanvasRenderer>();
        for (int i = 0; i < targetRenderer.Length; i++)
        {
            targetRenderer[i].gameObject.SetActive(true);
        }
    }
	
	// Update is called once per frame
	void Update () {
        if(start)
        {
            Fadeout(0);
            Fadeout(1);
            Fadeout(2);
        }
        if (targetRenderer[0].GetMaterial().color.a == 1)
            start = false;
    }
    private void Fadeout(int val)
    {
        //GUI.color = Color.Lerp(GUI.color, Color.clear, fadespeed * Time.deltaTime);
        targetRenderer[val].GetMaterial().color = Color.Lerp(targetRenderer[val].GetMaterial().color, toLerpSolid, 3f * Time.deltaTime);
    }
}
