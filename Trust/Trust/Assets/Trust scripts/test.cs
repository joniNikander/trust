﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class test : MonoBehaviour {
    public float fadeSpeed = 0.25f;          // Speed that the screen fades to and from black.
    private float maxvalue;
    private float speed;
    private float oldspeed;
    private float currentspeed;
    private bool sceneStarting = false;      // Whether or not the scene is still fading in.
    private Color OrigColor;
    
    void Start()
    {
        OrigColor = GetComponent<SpriteRenderer>().color;
        GameObject.FindGameObjectWithTag("Respawn").GetComponent<SpriteRenderer>().color = Color.clear;
        fadeSpeed= 0.25f;
    }

    // Update is called once per frame
    void Update()
    {
        if(sceneStarting)
        {
            maxvalue += 0.01f;
            fadeSpeed = maxvalue;
            EndScene();
        }
       
    }
    void Awake()
    {
        // Set the texture so that it is the the size of the screen and covers it.
        // GetComponent<SpriteRenderer>().pixelInset = new Rect(0, 0, Screen.width, Screen.height);

    }
    void FadeToClear()
    {
        // Lerp the colour of the texture between itself and transparent.
        if (GameObject.FindGameObjectWithTag("Respawn").GetComponent<SpriteRenderer>().color != Color.clear)
            GameObject.FindGameObjectWithTag("Respawn").GetComponent<SpriteRenderer>().color = Color.Lerp(GameObject.FindGameObjectWithTag("Respawn").GetComponent<SpriteRenderer>().color, Color.clear, 0.26f * Time.deltaTime);

    }

    public void setTrue(bool A)
    {
        sceneStarting = A;
    }
    void FadeToBlack()
    {
        // Lerp the colour of the texture between itself and black.

        Color old = GameObject.FindGameObjectWithTag("Respawn").GetComponent<SpriteRenderer>().color;
        GameObject.FindGameObjectWithTag("Respawn").GetComponent<SpriteRenderer>().color = Color.Lerp(old, Color.white, fadeSpeed * Time.deltaTime);
        // GetComponent<GUITexture>().color = Color.Lerp(GetComponent<GUITexture>().color, f, fadeSpeed * Time.deltaTime);
    }


    void StartScene()
    {
        // Fade the texture to clear.
        FadeToClear();

        // If the texture is almost clear...
        if (GameObject.FindGameObjectWithTag("Respawn").GetComponent<SpriteRenderer>().color.a <= 0.05f)
        {
            // ... set the colour to clear and disable the GUITexture.
            GameObject.FindGameObjectWithTag("Respawn").GetComponent<SpriteRenderer>().color = Color.clear;
            GameObject.FindGameObjectWithTag("Respawn").GetComponent<SpriteRenderer>().enabled = false;

            // The scene is no longer starting.
            sceneStarting = false;
        }
    }


    public void EndScene()
    {
        // Make sure the texture is enabled.
        GameObject.FindGameObjectWithTag("Respawn").GetComponent<SpriteRenderer>().enabled = true;
        // Start fading towards black.
        FadeToBlack();

        // If the screen is almost black...
        if (GameObject.FindGameObjectWithTag("Respawn").GetComponent<SpriteRenderer>().color.a >= 0.99f)
        {
            GameObject.FindGameObjectWithTag("Respawn").GetComponent<SpriteRenderer>().color = OrigColor;
            UnityEngine.SceneManagement.SceneManager.LoadScene("Corridor");
        }

    }
}