﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Spider : MonoBehaviour
{

    public bool Activate;
    public float Dice;
    public float Timer;
    private float Original_Timer;


    // Use this for initialization
    void Start()
    {


        Original_Timer = Timer;



        Activate = false;
    }

    // Update is called once per frame
    void Update()
    {
        

        if (Timer <= 0 )
        {
            Dice = Random.Range( 0f , 10 );
            Timer = Original_Timer;
        }
        else if(Timer > 0)
        {
            Activate = false;
        }
         Timer -= Time.deltaTime;

        if (Dice >= 9.0f)
        {
            Activate = true;
        }
        if(Input.GetKeyDown(KeyCode.S))
        {
            Activate = true;
        }
    }



    public void IsActive(bool p_bValue)
    {
        Activate = p_bValue;
    }
    public bool Active()
    {
        return Activate;
    }

}
