﻿using UnityEngine;
using System.Collections;

public class BalancingScript : MonoBehaviour
{
    public Sprite BalanceSprite;
    public bool moveForward; // autowalk
    public bool DebugControls;
    public GameObject targetToRotate;
    private GameObject[] Hands;
    private GameObject[] Hands2;
    public float amount;
    public float moveSpeed;
    public Vector3 targetPositionToMoveTo;
    private bool eventStart = false;
    private bool eventFinished = false;
    private bool rotatedTooMuch = false;
    private float forceAmount = 19f;
    private float appliedForceToPlayer = 0.03f;
    private float restrictionRotation = 0.55f;
    private float rotationInZ;
    Quaternion origrot;
    AudioSource Audio;
   public AudioClip Falling;
    private bool startEvent = false;
    private bool PlayFalling = false;
    // Use this for initialization
    void Start()
    {
        Hands = GameObject.FindGameObjectsWithTag("Hands");

        Hands2 = GameObject.FindGameObjectsWithTag("Hands2");

        Hands[0].GetComponent<Renderer>().enabled = false;
        Hands[1].GetComponent<Renderer>().enabled = false;

        Hands2[0].GetComponent<Renderer>().enabled = false;
        Hands2[1].GetComponent<Renderer>().enabled = false;

        origrot = transform.rotation;
        eventStart = false;
        eventFinished = false;
        rotatedTooMuch = false;
        Audio = this.GetComponent<AudioSource>();
    }
    // Update is called once per frame
    void Update()
    {
        if (eventStart)
        {
           // this.GetComponent<Animator>().enabled = false;
            //this.GetComponent<SpriteRenderer>().sprite = BalanceSprite;
           // this.GetComponent<Animator>().SetInteger("direction", 0);
           
            this.GetComponent<Animator>().SetInteger("direction", 100);
            this.GetComponent<movement_player_one>().enabled = false;
            for (int i = 0; i < 2; i++)
            {
                Hands[i].GetComponent<Renderer>().enabled = true;
                Hands2[i].GetComponent<Renderer>().enabled = true;
            }
                this.gameObject.GetComponent<movement_player_one>().SetMoveable(false);
                if (Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.LeftArrow) && Input.GetKey(KeyCode.RightArrow)
                || Input.GetKey(KeyCode.Joystick1Button3) && Input.GetKey(KeyCode.Joystick1Button1) && Input.GetKey(KeyCode.Joystick2Button3) && Input.GetKey(KeyCode.Joystick2Button1))
                {
                for (int i = 0; i < 1; i++)
                {
                    Hands[i].GetComponent<Renderer>().enabled = false;
                    Hands2[i].GetComponent<Renderer>().enabled = false;
                }
                this.gameObject.GetComponent<movement_player_one>().SetMoveable(true);
                    startEvent = true;
                }
            if(startEvent)
            {
                Balance();
                CheckRestrictions();
                if (rotationInZ >= 0.30f || rotationInZ <= -0.30f)
                {
                    if (!Audio.isPlaying)
                        PlayFalling = true;
                }
                if (rotationInZ >= restrictionRotation || rotationInZ <= -restrictionRotation)
                    PlayFalling = false;
                FallingSound();

            }
            if (eventFinished)
            {
                this.GetComponent<Animator>().SetInteger("direction", 1);
                this.GetComponent<movement_player_one>().enabled = true;
                this.GetComponent<Animator>().enabled = true;
            }

        }


    }
    void Balance()
    {
        if (transform.rotation.z < 0f)
            GetComponent<Animator>().SetInteger("direction", 102);
        else if(transform.rotation.z > 0f)
            GetComponent<Animator>().SetInteger("direction", 101);
        if (!rotatedTooMuch)
        {
            if (DebugControls)
            {
                if (Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.D)) // tryck ner A och vänster pil för att rotera åt vänster
                {
                    amount += forceAmount * Time.deltaTime;
                    appliedForceToPlayer = 0f;
                }
                else if (Input.GetKey(KeyCode.LeftArrow) && Input.GetKey(KeyCode.RightArrow)) // tryck ner D och höger pil för att rotera åt höger
                {
                    amount -= forceAmount * Time.deltaTime;
                    appliedForceToPlayer = 0f;
                }
                else
                {
                    appliedForceToPlayer += 0.1f * Time.deltaTime;
                    if (transform.rotation.z >  0.0f)
                    {
                        amount += appliedForceToPlayer;
                        Hands[0].GetComponent<Renderer>().enabled = false;
                        Hands[1].GetComponent<Renderer>().enabled = false;
                        Hands2[0].GetComponent<Renderer>().enabled = true;
                        Hands2[1].GetComponent<Renderer>().enabled = true;
                    }
                    else
                    {
                        amount -= appliedForceToPlayer;
                        Hands[0].GetComponent<Renderer>().enabled = true;
                        Hands[1].GetComponent<Renderer>().enabled = true;
                        Hands2[0].GetComponent<Renderer>().enabled = false;
                        Hands2[1].GetComponent<Renderer>().enabled = false;
                    }


                }
                transform.RotateAround(targetToRotate.transform.position, Vector3.forward, amount * Time.deltaTime); // rotation i z runt object x position till vänster
                if (moveForward)
                    transform.position = Vector3.MoveTowards(transform.position, transform.position + targetPositionToMoveTo, moveSpeed * Time.deltaTime); // förflytta spelar objekt mot vector TargetPositionToMove med hastighet moveSpeed
                if (transform.position.z >= targetPositionToMoveTo.z)
                    eventStart = false;
            }
            else if (!DebugControls)
            {
                if (Input.GetKey(KeyCode.Joystick2Button3) && Input.GetKey(KeyCode.Joystick2Button1)) // tryck ner A och vänster pil för att rotera åt vänster
                {
                    amount += forceAmount * Time.deltaTime;
                    appliedForceToPlayer = 0f;
                }
                else if (Input.GetKey(KeyCode.Joystick1Button3) && Input.GetKey(KeyCode.Joystick1Button1)) // tryck ner D och höger pil för att rotera åt höger
                {
                    amount -= forceAmount * Time.deltaTime;
                    appliedForceToPlayer = 0f;
                }
                else
                {
                    appliedForceToPlayer += Time.deltaTime;
                    if (transform.rotation.z >= 0f)
                    {
                        amount += appliedForceToPlayer;
                        Hands[0].GetComponent<Renderer>().enabled = true;
                        Hands[1].GetComponent<Renderer>().enabled = true;
                        Hands2[0].GetComponent<Renderer>().enabled = false;
                        Hands2[1].GetComponent<Renderer>().enabled = false;
                    }
                    else
                    {
                        amount -= appliedForceToPlayer;

                        Hands[0].GetComponent<Renderer>().enabled = false;
                        Hands[1].GetComponent<Renderer>().enabled = false;
                        Hands2[0].GetComponent<Renderer>().enabled = true;
                        Hands2[1].GetComponent<Renderer>().enabled = true;
                    }

                }
                transform.RotateAround(targetToRotate.transform.position, Vector3.forward, amount * Time.deltaTime); // rotation i z runt object x position till vänster
                if (moveForward)
                {
                    transform.position = Vector3.MoveTowards(transform.position, transform.position + targetPositionToMoveTo, moveSpeed * Time.deltaTime); // förflytta spelar objekt mot vector TargetPositionToMove med hastighet moveSpeed
                }                  
                if (transform.position.z >= targetPositionToMoveTo.z)
                    eventStart = false;
            }
        }

    }
    private void CheckRestrictions()
    {
        rotationInZ = transform.rotation.z;
        if (rotationInZ >= restrictionRotation)//kolla om du roterat för mycket. Ifall sant använd gravitation på rigidbody för spelare och ge den en negativ y kraft.
        {
            
            rotatedTooMuch = true;
          
            GetComponent<Rigidbody>().useGravity = true;
            GetComponent<Rigidbody>().AddForce(new Vector3(0f, -10f * Time.fixedDeltaTime, 0f));
            if (transform.position.y <= -50f)
            {
                GetComponent<Rigidbody>().useGravity = false;
                UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
            }
           
        }
        if (rotationInZ <= -restrictionRotation)//kolla om du roterat för mycket. Ifall sant använd gravitation på rigidbody för spelare och ge den en negativ y kraft.
        {
            
            rotatedTooMuch = true;
           
            GetComponent<Rigidbody>().useGravity = true;
            GetComponent<Rigidbody>().AddForce(new Vector3(0f, -10f * Time.fixedDeltaTime, 0f));
            if (transform.position.y <= -50f)
            {
                
                GetComponent<Rigidbody>().useGravity = false;
                UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
            }
           
        }
        if (transform.position.z >= targetPositionToMoveTo.z)
        {
            eventFinished = true;
            Hands[0].GetComponent<Renderer>().enabled = false;
            Hands[1].GetComponent<Renderer>().enabled = false;

            Hands2[0].GetComponent<Renderer>().enabled = false;
            Hands2[1].GetComponent<Renderer>().enabled = false;
            eventStart = false;
            moveForward = false;
            GetComponent<movement_player_one>().SetMovingFear(true);
            transform.rotation = origrot;
        }
    }
    public void SetEvents(bool val)
    {
        eventStart = val;
    }

    public void FallingSound()
    {

        if (PlayFalling == true)
        {
            Debug.Log("Falling");
            Audio.PlayOneShot(Falling, 1);
            PlayFalling = false;
        }
    }
}