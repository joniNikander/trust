﻿using UnityEngine;
using System.Collections;

public class BrokenStick : MonoBehaviour {

    public Sprite Broken;



	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.tag == "Player_One")
        {
            this.GetComponent<SpriteRenderer>().sprite = Broken;
        }
    }
}
