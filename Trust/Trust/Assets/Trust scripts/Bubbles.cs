﻿using UnityEngine;
using System.Collections;

public class Bubbles : MonoBehaviour {

    public bool Moveable;
    public Transform Target;
    public Transform Target1;
    public Transform Target2;
    public Transform Target3;
    public Transform Target4;
    public Transform Target5;
   
  

    private GameObject Troll;

    private bool Stone;
    private bool Stone1;
    private bool Stone2;
    private bool Stone3;
    private bool Stone4;
    private bool Stone5;

   

    public float Speed;
    //public float Timer;

    private int Puzzle;
    private Animator Bubble;


    // Use this for initialization
    void Start()
    {

  

        Bubble = this.gameObject.GetComponent<Animator>();

        Bubble.enabled = true;
        Moveable = true;
        Stone = true;
        Stone1 = false;
        Stone2 = false;
        Stone3 = false;
        Stone4 = false;
        Stone5 = false;
      




    }

    // Update is called once per frame
    void Update()
    {
        float step = Speed * Time.deltaTime;

        //Timer -= Time.deltaTime;






        if (Moveable == true)
        {
            if (Stone == true)
            {
                Vector3 Target1 = new Vector3(Target.position.x, transform.position.y, Target.position.z);

                transform.position = Vector3.MoveTowards(transform.position, Target1, step);
            }
            if (Stone1 == true && Stone == false)
            {
                Vector3 Target2 = new Vector3(Target1.position.x, transform.position.y, Target1.position.z);
                transform.position = Vector3.MoveTowards(transform.position, Target2, step);
            }
            if (Stone2 == true && Stone1 == false)
            {
                Vector3 Target3 = new Vector3(Target2.position.x, transform.position.y, Target2.position.z);
                transform.position = Vector3.MoveTowards(transform.position, Target3, step);
            }
            if (Stone3 == true && Stone2 == false)
            {
                Vector3 Target4 = new Vector3(Target3.position.x, transform.position.y, Target3.position.z);
                transform.position = Vector3.MoveTowards(transform.position, Target4, step);

            }
            if (Stone4 == true && Stone3 == false)
            {
                Vector3 Target5 = new Vector3(Target4.position.x, transform.position.y, Target4.position.z);
                transform.position = Vector3.MoveTowards(transform.position, Target5, step);
            }
             if (Stone5 == true && Stone4 == false)
             {
                 transform.position = Vector3.MoveTowards(transform.position, Target5.position, step);
             }
            
        }




    }
    void OnTriggerEnter(Collider col)
    {

        if (col.gameObject.name == "Stone")
        {
            Stone1 = true;
            Stone = false;
            Moveable = false;
        



        }
        if (col.gameObject.name == "StoneTwo")
        {
            Stone2 = true;
            Stone1 = false;
            Moveable = false;

        }
        if (col.gameObject.name == "StoneThree")
        {
            Stone3 = true;
            Stone2 = false;
            Moveable = false;



        }
        if (col.gameObject.name == "StoneFour")
        {
            Stone4 = true;
            Stone3 = false;
            Moveable = false;

        }
        if (col.gameObject.name == "StoneFive")
        {
   
            Stone4 = false;
            Stone5 = true;
            Moveable = false;

        }
        if (col.gameObject.name == "StoneSix")
        {

            Stone5 = false;

        }
    }

    public void SetMovable(bool p_bValue)
    {
        Moveable = p_bValue;
    }


}
