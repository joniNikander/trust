﻿
using UnityEngine;
using System.Collections;

public class MusicTriggers : MonoBehaviour {

    public enum TrackType { loop, stinger };
    public TrackType trackType;

    public Color activationColor;
    public uint zone_id = 0;

    public delegate void ZoneTriggered(uint zone_id, TrackType trackType);
    public delegate void ThemeLoaded(ref Elias.Theme theme);

    // All interested listeners should register to this event.
    // e.g MusicSwitcher.Switch(uint zone_id)
    public static event ZoneTriggered OnZoneTriggered;
    public static event ThemeLoaded OnThemeLoaded;

    public enum ELIASTriggerType { TriggerEnter, TriggerExit, CollisionEnter, CollisionExit };

    /* I'm not sure that action should be decided by the trigger.
	 * On the other hand, it could be of great use when designing a level.
	 * Example: Suddlenly one decides that music should stop when triggered,
	 *  or that a stinger should play instead of level change. But, what stinger?
	 * Such information has to be supplied here too in such case 
	 * (or in another well defined component on the same level and game object).
	 */
    //enum ELIASAction {SwitchLevel, PlayStinger};
    public ELIASTriggerType TriggerOn = ELIASTriggerType.TriggerEnter;

    private bool hasChangedName;
    private bool hasTriggered;


    public static void ThemeWasLoaded(ref Elias.Theme theme)
    {
        if (OnThemeLoaded != null)
        {
            OnThemeLoaded(ref theme);
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        // Send trigger event. Let event manager on ELIAS_Component be a registered event listener.
       //if (collider.gameObject.tag == ("Player_One")) { 

        if (TriggerOn == ELIASTriggerType.TriggerEnter)
        {

            // Call all registered methods for this event!
            if (OnZoneTriggered != null)
            { 
                Debug.Log("Wuuuut");
                OnZoneTriggered(zone_id, trackType);
                hasTriggered = true;
          }}

        
    }

    void OnTriggerExit(Collider collider)
    {
        Gizmos.color = trackType == TrackType.loop ? Color.blue : Color.cyan;
        hasTriggered = false;
    }
    [ExecuteInEditMode]
    void OnDrawGizmos()
    {
        Gizmos.color = trackType == TrackType.loop ? Color.blue : Color.cyan;
        if (hasTriggered)
            Gizmos.color = Color.red;
        
        Gizmos.DrawWireCube(GetComponent<BoxCollider>().bounds.center, GetComponent<BoxCollider>().bounds.size);
    }
}
