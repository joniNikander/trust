﻿using UnityEngine;
using System.Collections;

public class BarCounter : MonoBehaviour {

    public GameObject DanceObject;

    public int NumberOfBar;
    public float Countdown = 5;
    public bool DanceLeft;
    public bool DanceRight;
    public bool DanceForward;
    private bool DanceSuccess;
    private bool DanceFail;

    public int BarCounts(ref Elias.Theme CursorPositionBeats)
    {
        return NumberOfBar;
    }
    public delegate void ThemeLoaded(ref Elias.Theme theme);
    public static event ThemeLoaded OnThemeLoaded;

    public static void ThemeWasLoaded(ref Elias.Theme theme)
    {
        if (OnThemeLoaded != null)
        {
            OnThemeLoaded(ref theme);
        }
    }

    void DanceMoves()
    {

        if (DanceLeft == true)
        {

            Countdown -= Time.deltaTime;
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                DanceSuccess = true;
                Countdown = 5;
            }
            else if (Countdown == 0)
            {
                DanceFail = true;
            }
             if (DanceRight == true)
            {

                Countdown -= Time.deltaTime;
                if (Input.GetKey(KeyCode.RightArrow))
                {
                    DanceSuccess = true;
                    Countdown = 5;
                    
                }
                else if (Countdown == 0)
                {
                    DanceFail = true;
                }

                if (DanceSuccess == true)
                {
                    Destroy (gameObject, 5);
                }
            }
        }

    }
    // Use this for initialization
    void Start () {
	
    

	}
	
	// Update is called once per frame
	void Update () {
        if (NumberOfBar == 0)
        {
            DanceMoves();
        }
	
	}
}
