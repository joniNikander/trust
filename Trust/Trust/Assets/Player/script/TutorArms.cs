﻿using UnityEngine;
using System.Collections;

public class TutorArms : MonoBehaviour {

    private GameObject[] Arms;

    // Use this for initialization
    void Start () {


        Arms = GameObject.FindGameObjectsWithTag("TutArm");

        Arms[0].GetComponent<Animator>().enabled = false;
        Arms[1].GetComponent<Animator>().enabled = false;
        Arms[0].GetComponent<Renderer>().enabled = false;
        Arms[1].GetComponent<Renderer>().enabled = false;
     
        

    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.name == "StoneFour")
        {

            Arms[0].GetComponent<Animator>().enabled = true;
            Arms[1].GetComponent<Animator>().enabled = true;
            Arms[0].GetComponent<Renderer>().enabled = true;
            Arms[1].GetComponent<Renderer>().enabled = true;
        }

        if (col.gameObject.name == "StoneFive")
        {

            Arms[0].GetComponent<Animator>().enabled = false;
            Arms[1].GetComponent<Animator>().enabled = false;
            Arms[0].GetComponent<Renderer>().enabled = false;
            Arms[1].GetComponent<Renderer>().enabled = false;
        }
    }
}
