﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class Fear : MonoBehaviour {

    public float FearMinimumValue = 0.0f;
    public float FearMaximumValue = 200.0f;
    public float CurrentFearValue = 0.0f;
    public float FearIncrementTimer = 5.0f;
    public float OriginalTimerTime;
    private float FearReductionValue = 5.0f;
    private float FearIncrementValue = 0.25f;

    private GameObject target;

	// Use this for initialization
	void Start () {
        OriginalTimerTime = FearIncrementTimer;
        target = this.gameObject;

    }
	
	// Update is called once per frame
	void Update () {

        FearIncrementTimer -= Time.deltaTime;

        if (FearIncrementTimer <= 0)
        {
            CurrentFearValue += FearIncrementValue;
            FearIncrementTimer = OriginalTimerTime;
        }
        if (CurrentFearValue >= FearMaximumValue)
            SceneManager.LoadScene("Menu_Start");

        if (target.GetComponent<movement_player_one>().IsHugging())
        {
            target.GetComponent<movement_player_one>().SetMovingFear(false);
            if(CurrentFearValue >= 0f)
            {
                CurrentFearValue -= FearReductionValue;

            }

            else if (CurrentFearValue <= 0f)
                CurrentFearValue = 0f;      
        }
        else if (!target.GetComponent<movement_player_one>().IsHugging())
            target.GetComponent<movement_player_one>().SetMovingFear(true);
    }
    public float GetCurrentFear()
    {
        return CurrentFearValue;
    }
    public void IncreaseFear()
    {
        CurrentFearValue += FearIncrementValue;
    }
    public void SetCurrentFear(float val)
    {
        CurrentFearValue += val * Time.deltaTime;
    }

}
