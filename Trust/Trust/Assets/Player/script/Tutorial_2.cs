﻿using UnityEngine;
using System.Collections;

public class Tutorial_2 : MonoBehaviour {

    public GameObject Player;
    private GameObject[] Hands;
    public bool moveLeft = false;
    private bool moveRight = false;
    private bool rightHand = false;
    private Vector3 newposLeft;
    private Vector3 newposRight;

    private Color newcolor;
    private Color newcolor2;
    private Color Startcolor;
    private bool ColorFade;
    private bool ColorFade2;


    // Use this for initialization
    void Start () {

      

        GetComponent<Tutorial_2>().enabled = false;
        GetComponentInChildren<Light>().enabled = false;
        Player.GetComponent<Animator>().SetInteger("tut", 0);

        Hands = GameObject.FindGameObjectsWithTag("Hands");

        newcolor = Hands[0].GetComponent<Renderer>().material.color;
        newcolor2 = Hands[1].GetComponent<Renderer>().material.color;


    }
	
	// Update is called once per frame
	void Update () {
        if (moveLeft == false && moveRight == false)
        {
            Hands[0].GetComponent<Renderer>().enabled = true;


        }

        if (moveRight == false && moveLeft == true)
        {
            rightHand = true;

        }

        if (rightHand == true)
        {
            Hands[1].GetComponent<Renderer>().enabled = true;
         
        }
     
            
          


        if (Player.GetComponent<Animator>().GetInteger("tut") != 0)
            Player.GetComponent<Animator>().SetInteger("direction", 0);
        else if(Player.GetComponent<Animator>().GetInteger("direction") == 1)
            Player.GetComponent<Animator>().SetInteger("tut", 0);

        if (Player.GetComponent<Animator>().GetInteger("tut") > 0 && Player.GetComponent<Animator>().GetInteger("direction") == 0)
        {
            Player.GetComponent<movement_player_one>().enabled = false;
        }
        else if (Player.GetComponent<Animator>().GetInteger("direction") > 0 && Player.GetComponent<Animator>().GetInteger("tut") == 0)
            Player.GetComponent<movement_player_one>().enabled = true;

        if (Player.GetComponent<Animator>().GetInteger("tut") == 2)
        { 
            if (Player.GetComponent<Animator>().GetInteger("tut") == 2 && Input.GetKeyDown(KeyCode.LeftArrow) || Player.GetComponent<Animator>().GetInteger("tut") == 2 && Input.GetKeyDown(KeyCode.Joystick1Button3))
            {
                moveLeft = true;
                ColorFade = true;

          

            }
        }
        if (moveLeft)
        {            
            Player.transform.position = Vector3.MoveTowards(Player.transform.position, newposLeft, 2.33f * Time.deltaTime);
            if (Player.transform.position == newposLeft)
            {
                Player.GetComponent<Animator>().SetInteger("tut", 3);
            }
        }
        if(Player.GetComponent<Animator>().GetInteger("tut") == 3)
        {           
            moveLeft = false;
            if (Player.GetComponent<Animator>().GetInteger("tut") == 3 && Input.GetKeyDown(KeyCode.RightArrow) || Player.GetComponent<Animator>().GetInteger("tut") == 3 && Input.GetKeyDown(KeyCode.Joystick1Button1))
            {
                moveRight = true;
                ColorFade2 = true;
                
            }
        }
        if (moveRight)
        {
            Player.transform.position = Vector3.MoveTowards(Player.transform.position, newposRight, 2.33f * Time.deltaTime);
            if (Player.transform.position == newposRight)
            {
                Player.GetComponent<Animator>().SetInteger("direction", 1);
                Player.GetComponent<Animator>().SetInteger("tut", 0);
                moveRight = false;
            }
        }

        if (ColorFade == true)
        {
            newcolor.a -= 1f * Time.deltaTime;

            Hands[0].GetComponent<Renderer>().material.color = newcolor;
         
        }

        if (Hands[0].GetComponent<Renderer>().material.color.a <= 0)
        {
         
            if (newcolor == Startcolor)
            {
                Hands[0].GetComponent<Renderer>().enabled = false;
                ColorFade = false;

            }

        }
        if (ColorFade2 == true)
        {
            newcolor2.a -= 1f * Time.deltaTime;
            Hands[1].GetComponent<Renderer>().material.color = newcolor2;
        }

        if (Hands[1].GetComponent<Renderer>().material.color.a <= 0)
        {
            newcolor2 = Startcolor;

            if (newcolor2 == Startcolor)
            {
                Hands[1].GetComponent<Renderer>().enabled = false;
                ColorFade2 = false;

            }

        }

     
    }

    void OnTriggerEnter(Collider col)
    {       
        if (col.gameObject.tag == "Player_One")
        {
            GetComponentInChildren<Light>().enabled = true;

            newposLeft = new Vector3(Player.transform.position.x - 1.33f, Player.transform.position.y, Player.transform.position.z + 1.33f);
            newposRight = new Vector3(newposLeft.x + 1.33f, newposLeft.y, newposLeft.z + 1.33f);

            GetComponent<Tutorial_2>().enabled = true;

            Player.GetComponent<Animator>().SetInteger("tut", 2);
            Player.GetComponent<Animator>().SetInteger("direction", 0);
        }
    }
    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player_One")
        {

            GetComponentInChildren<Light>().enabled = false;
            Player.GetComponent<Animator>().SetInteger("tut", 0);
            GetComponent<Tutorial_2>().enabled = false;
        }

    }
}
