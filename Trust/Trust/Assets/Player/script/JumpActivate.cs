﻿using UnityEngine;
using System.Collections;

public class JumpActivate : MonoBehaviour
{

    public GameObject PlayerOne;
    public GameObject PlayerTwo;
    public GameObject Player;
    public Camera mainCamera;
    public Camera secondaryCamera;
    // Use this for initialization
    void Start()
    {
        PlayerOne.SetActive(false);
        PlayerTwo.SetActive(false);
        PlayerOne.GetComponent<SpriteRenderer>().enabled = false;
        PlayerTwo.GetComponent<SpriteRenderer>().enabled = false;
        secondaryCamera.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name == "Player_One")
        {
            PlayerOne.GetComponent<SpriteRenderer>().enabled = true;
            PlayerTwo.GetComponent<SpriteRenderer>().enabled = true;

            PlayerOne.SetActive(true);
            PlayerTwo.SetActive(true);
            Player.SetActive(false);

            Player.GetComponent<movement_player_one>().SetMoveable(false);
            Player.GetComponent<SpriteRenderer>().enabled = false;

            mainCamera.enabled = false;
            secondaryCamera.enabled = true;
        }
    }
}
