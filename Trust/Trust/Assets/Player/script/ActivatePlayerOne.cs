﻿using UnityEngine;
using System.Collections;

public class ActivatePlayerOne : MonoBehaviour {
    public GameObject PlayerOne;
    public GameObject JumpPlayerOne;
    public GameObject JumpPlayerTwo;
    private GameObject []Arms;
    public Camera mainCamera;
    public Camera secondaryCamera;
    // Use this for initialization
    void Start () {

        Arms = GameObject.FindGameObjectsWithTag("Arm");



    }
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnTriggerEnter(Collider col)
    {
        if(col.name == "PlayerTwo")
        {
            if(PlayerOne.transform.position != JumpPlayerOne.transform.position)
                PlayerOne.transform.position = JumpPlayerOne.transform.position;

            Arms[0].GetComponent<WaterArms>().IsActive(false);
            Arms[1].GetComponent<WaterArms>().IsActive(false);
            Arms[0].GetComponent<Renderer>().enabled = false;
            Arms[1].GetComponent<Renderer>().enabled = false;

            PlayerOne.SetActive(true);
            PlayerOne.GetComponent<SpriteRenderer>().enabled = true;
            PlayerOne.GetComponent<movement_player_one>().SetMoveable(true);

            JumpPlayerOne.GetComponent<SpriteRenderer>().enabled = false;
            JumpPlayerOne.SetActive(false);

            JumpPlayerTwo.GetComponent<SpriteRenderer>().enabled = false;
            JumpPlayerTwo.SetActive(false);

            secondaryCamera.enabled = false;
            mainCamera.enabled = true;
        }
    }
}
