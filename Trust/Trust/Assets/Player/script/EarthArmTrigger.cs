﻿using UnityEngine;
using System.Collections;

public class EarthArmTrigger : MonoBehaviour {

    private GameObject Arm;
    public GameObject Player;
    public float Timer;
    private float Original_Timer;
    public AudioClip ScarySound;
    AudioSource Audio;
    private bool ScareBool = false;
    public float LeTimer;
    private bool TimerStarto = false;
    public Animator Finger;
    // Use this for initialization
    void Start () {

       Finger = Finger.GetComponent<Animator>();
        Arm = GameObject.FindGameObjectWithTag("Arm");
      
        Arm.GetComponent<Animator>().enabled = false;

        Original_Timer = Timer;
        Audio = GetComponent<AudioSource>();
        Finger.SetBool("FingerTrigger", false);

    }
	
	// Update is called once per frame
	void Update () {



        /* if(Timer <= 0)
         {
             Player.GetComponent<Fear>().SetCurrentFear(55f);
         }*/
         if (TimerStarto == true)
        {
            LeTimer += Time.deltaTime;
        }
        if(LeTimer >= 3.5f)
        {
        Finger.SetBool("FingerTrigger", true);
        }





    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name == "Player_One")
        {

            TimerStarto = true;
            ScareBool = true;
            Arm.GetComponent<Renderer>().enabled = true;
            Arm.GetComponent<Animator>().enabled = true;

            Player.GetComponent<movement_player_one>().enabled = false;
            if (ScareBool)
            {
                ScareSound();
            }
        }
    }

    void OnTriggerStay(Collider col)
    {
        if (col.gameObject.name == "Player_One")
        {
      
            Timer -= Time.deltaTime;
            Player.GetComponent<Fear>().SetCurrentFear(10f);

            if (Input.GetKey(KeyCode.Joystick1Button3) && Input.GetKey(KeyCode.Joystick1Button1) && Input.GetKey(KeyCode.Joystick2Button3) && Input.GetKey(KeyCode.Joystick2Button1) || Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.LeftArrow) && Input.GetKey(KeyCode.RightArrow))
            {
                Player.GetComponent<movement_player_one>().enabled = true;

                Timer = Original_Timer;
              //  this.enabled = false;


            }
           
        }
    }

    void ScareSound()
    {
        Audio.PlayOneShot(ScarySound, 1);
        ScareBool = false;

    }
}
