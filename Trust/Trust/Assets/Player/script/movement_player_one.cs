﻿using UnityEngine;
using System.Collections;

public class movement_player_one : MonoBehaviour {
    public Vector3 MoveSpeed;
    public bool DebugControls;
    public AudioClip girlScared;
    public AudioClip boyScared;
    public AudioClip Grass;
    

    public Vector3 TargetSpeed;
    public Vector3 AccelerationSpeed;
    private float checkFear = 17.0f;
    private float origTimer;
    private int is_moving = 0;
    private bool Moveable = true;
    private bool Idle = false;
    private bool IsPlayerHugging = false;
    private bool PlayerOneControl = true;
    public bool movingForward = true;
    private bool playScared = false;
    private bool playGrass = false;
    private Vector3 m_CurrentVelocity;
    private Vector3 orig_move_speed;
    private Vector3 orig_acceleration_speed;
    private Animator anim;
    //private Animator anim;

    // Use this for initialization
    void Start () {
        anim = this.GetComponent<Animator>();
        orig_acceleration_speed = AccelerationSpeed;
        orig_move_speed = MoveSpeed;
        origTimer = checkFear;
        TargetSpeed = new Vector3(4.33f, 1.33f, 4.33f);
        AccelerationSpeed = new Vector3(0.33f, 0.33f, 0.33f);
    }

    // Update is called once per frame
    void Update()
    {
        if (!Input.anyKey)
        {
            IsPlayerHugging = false;
            if (!IsPlayerHugging)
            {
                SetMoveable(true);
                anim.SetBool("isboyhugging", false);
                anim.SetBool("isgirlhugging", false);
            }

        }
        FearCheck();
        is_moving = 0;
        if (!DebugControls)
        {
            JoystickControls();      
        }
        else if (DebugControls)
        {
            KeyboardControls();
        }       
    }
   private void KeyboardControls()
    {
        if (movingForward)
        {
            if (PlayerOneControl)
            {
                if (Moveable)
                {
                    if (Idle == true)
                        anim.SetBool("idle", true);
                    if (!Input.anyKey)
                    {
                        anim.SetInteger("direction", 0);
                        Idle = true;
                        IsPlayerHugging = false;
                        AccelerationSpeed = orig_acceleration_speed;
                        MoveSpeed = orig_move_speed;
                    }
                    else if (Input.anyKey)
                    {
                        anim.SetBool("idle", false);
                        if (!IsPlayerHugging)
                        {
                            anim.SetBool("isboyhugging", false);
                            anim.SetBool("isgirlhugging", false);
                        }
                        anim.SetBool("girlidle", false);
                        Idle = false;
                    }
                    if (Input.GetKey(KeyCode.LeftArrow))
                    {//yolo
                        anim.SetInteger("direction", 3);
                        is_moving = 1;
                        float temp_x = MoveSpeed.x;
                        temp_x += AccelerationSpeed.x;
                        MoveSpeed.x = temp_x;
                        float temp_z = MoveSpeed.y;
                        temp_z += AccelerationSpeed.y;
                        MoveSpeed.y = temp_z;
                        transform.Translate(-MoveSpeed.x * Time.deltaTime, 0.0f, MoveSpeed.y * Time.deltaTime);
                    }
                    if (Input.GetKey(KeyCode.RightArrow))
                    {
                        anim.SetInteger("direction", 4);
                        is_moving = 2;
                        float temp_x = MoveSpeed.x;
                        temp_x += AccelerationSpeed.x;
                        MoveSpeed.x = temp_x;
                        float temp_z = MoveSpeed.y;
                        temp_z += AccelerationSpeed.y;
                        MoveSpeed.y = temp_z;
                        transform.Translate(MoveSpeed.x * Time.deltaTime, 0.0f, MoveSpeed.y * Time.deltaTime);
                    }
                    if (Input.GetKey(KeyCode.LeftArrow) && Input.GetKey(KeyCode.RightArrow))
                    {
                        anim.SetInteger("direction", 1);
                        is_moving = 3;
                        float temp_z = MoveSpeed.z;
                        temp_z += AccelerationSpeed.z;
                        MoveSpeed.z = temp_z;
                        transform.Translate(0.0f, 0.0f, MoveSpeed.z * Time.deltaTime);
                    }
                    if (MoveSpeed.x >= TargetSpeed.x)
                    {
                        MoveSpeed.x = TargetSpeed.x;
                    }
                    if (MoveSpeed.y >= TargetSpeed.y)
                    {
                        MoveSpeed.y = TargetSpeed.y;
                    }
                    if (MoveSpeed.z >= TargetSpeed.z)
                    {
                        MoveSpeed.z = TargetSpeed.z;
                    }

                    //if (Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.D) && PlayerOneControl && anim.GetBool("boyscared"))
                    //{
                    //        PlayerOneControl = false;
                    //        anim.SetBool("boyscared", false);
                    //        //anim.SetInteger("direction", 1);
                    //}
                }
                if (Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.LeftArrow) && Input.GetKey(KeyCode.RightArrow) && !IsPlayerHugging)
                {
                    IsPlayerHugging = true;
                    Moveable = false;
                    anim.SetBool("isboyhugging", true);
                }
            }
            if (!PlayerOneControl)
            {
                if (Moveable)
                {
                    if (Idle == true)
                        anim.SetBool("girlidle", true);
                    if (!Input.anyKey)
                    {
                        anim.SetInteger("direction", 0);
                        Idle = true;
                        IsPlayerHugging = false;
                        AccelerationSpeed = orig_acceleration_speed;
                        MoveSpeed = orig_move_speed;
                    }
                    else if (Input.anyKey)
                    {
                        anim.SetBool("idle", false);
                        if (!IsPlayerHugging)
                        {
                            anim.SetBool("isboyhugging", false);
                            anim.SetBool("isgirlhugging", false);
                        }
                        anim.SetBool("girlidle", false);
                        Idle = false;
                    }
                    if (Input.GetKey(KeyCode.A))
                    {
                        anim.SetInteger("direction", 5);
                        is_moving = 1;
                        float temp_x = MoveSpeed.x;
                        temp_x += AccelerationSpeed.x;
                        MoveSpeed.x = temp_x;
                        float temp_z = MoveSpeed.y;
                        temp_z += AccelerationSpeed.y;
                        MoveSpeed.y = temp_z;
                        transform.Translate(-MoveSpeed.x * Time.deltaTime, 0.0f, MoveSpeed.y * Time.deltaTime);
                    }
                    if (Input.GetKey(KeyCode.D))
                    {
                        anim.SetInteger("direction", 6);
                        is_moving = 2;
                        float temp_x = MoveSpeed.x;
                        temp_x += AccelerationSpeed.x;
                        MoveSpeed.x = temp_x;
                        float temp_z = MoveSpeed.y;
                        temp_z += AccelerationSpeed.y;
                        MoveSpeed.y = temp_z;
                        transform.Translate(MoveSpeed.x * Time.deltaTime, 0.0f, MoveSpeed.y * Time.deltaTime);
                    }
                    if (Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.D))
                    {
                        anim.SetInteger("direction", 2);
                        is_moving = 3;
                        float temp_z = MoveSpeed.z;
                        temp_z += AccelerationSpeed.z;
                        MoveSpeed.z = temp_z;
                        transform.Translate(0.0f, 0.0f, MoveSpeed.z * Time.deltaTime);
                    }
                    if (MoveSpeed.x >= TargetSpeed.x)
                    {
                        MoveSpeed.x = TargetSpeed.x;
                    }
                    if (MoveSpeed.y >= TargetSpeed.y)
                    {
                        MoveSpeed.y = TargetSpeed.y;
                    }
                    if (MoveSpeed.z >= TargetSpeed.z)
                    {
                        MoveSpeed.z = TargetSpeed.z;
                    }

                    //if (Input.GetKey(KeyCode.LeftArrow) && Input.GetKey(KeyCode.RightArrow) && !PlayerOneControl && anim.GetBool("girlscared"))
                    //{
                    //        PlayerOneControl = true;
                    //        anim.SetBool("girlscared", false);
                    //        anim.SetInteger("direction", 2);
                    //}
                }
                if (Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.LeftArrow) && Input.GetKey(KeyCode.RightArrow) && !IsPlayerHugging)
                {
                    IsPlayerHugging = true;
                    Moveable = false;
                    anim.SetBool("isgirlhugging", true);
                }
            }
        }
    }
    private void JoystickControls()
    {
        if (movingForward)
        {
            if (PlayerOneControl)
            {
                if (Moveable)
                {
                    if (Idle == true)
                        anim.SetBool("idle", true);
                    if (!Input.anyKey)
                    {
                        anim.SetInteger("direction", 0);
                        Idle = true;
                        IsPlayerHugging = false;
                        AccelerationSpeed = orig_acceleration_speed;
                        MoveSpeed = orig_move_speed;
                    }
                    else if (Input.anyKey)
                    {
                        anim.SetBool("idle", false);
                        if (!IsPlayerHugging)
                        {
                            anim.SetBool("isboyhugging", false);
                            anim.SetBool("isgirlhugging", false);
                        }
                        anim.SetBool("girlidle", false);
                        Idle = false;
                    }
                    if (Input.GetKey(KeyCode.Joystick1Button3))
                    {//yolo
                        anim.SetInteger("direction", 3);
                        is_moving = 1;
                        float temp_x = MoveSpeed.x;
                        temp_x += AccelerationSpeed.x;
                        MoveSpeed.x = temp_x;
                        float temp_z = MoveSpeed.y;
                        temp_z += AccelerationSpeed.y;
                        MoveSpeed.y = temp_z;
                        transform.Translate(-MoveSpeed.x * Time.deltaTime, 0.0f, MoveSpeed.y * Time.deltaTime);
                    }
                    if (Input.GetKey(KeyCode.Joystick1Button1))
                    {
                        anim.SetInteger("direction", 4);
                        is_moving = 2;
                        float temp_x = MoveSpeed.x;
                        temp_x += AccelerationSpeed.x;
                        MoveSpeed.x = temp_x;
                        float temp_z = MoveSpeed.y;
                        temp_z += AccelerationSpeed.y;
                        MoveSpeed.y = temp_z;
                        transform.Translate(MoveSpeed.x * Time.deltaTime, 0.0f, MoveSpeed.y * Time.deltaTime);
                    }
                    if (Input.GetKey(KeyCode.Joystick1Button3) && Input.GetKey(KeyCode.Joystick1Button1))
                    {
                        anim.SetInteger("direction", 1);
                        is_moving = 3;
                        float temp_z = MoveSpeed.z;
                        temp_z += AccelerationSpeed.z;
                        MoveSpeed.z = temp_z;
                        transform.Translate(0.0f, 0.0f, MoveSpeed.z * Time.deltaTime);
                    }
                    if (MoveSpeed.x >= TargetSpeed.x)
                    {
                        MoveSpeed.x = TargetSpeed.x;
                    }
                    if (MoveSpeed.y >= TargetSpeed.y)
                    {
                        MoveSpeed.y = TargetSpeed.y;
                    }
                    if (MoveSpeed.z >= TargetSpeed.z)
                    {
                        MoveSpeed.z = TargetSpeed.z;
                    }

                    //if (Input.GetKey(KeyCode.Joystick2Button0) && Input.GetKey(KeyCode.Joystick2Button1) && PlayerOneControl && anim.GetBool("boyscared"))
                    //{
                    //    PlayerOneControl = false;
                    //    anim.SetBool("boyscared", false);
                    //    //anim.SetInteger("direction", 1);
                    //}
                }
                if (Input.GetKey(KeyCode.Joystick2Button3) && Input.GetKey(KeyCode.Joystick2Button1) && Input.GetKey(KeyCode.Joystick1Button3) && Input.GetKey(KeyCode.Joystick1Button1) && !IsPlayerHugging)
                {
                    IsPlayerHugging = true;
                    //Moveable = false;
                    anim.SetBool("isboyhugging", true);
                }
            }
            if (!PlayerOneControl)
            {
                if (Moveable)
                {
                    if (Idle == true)
                        anim.SetBool("girlidle", true);
                    if (!Input.anyKey)
                    {
                        anim.SetInteger("direction", 0);
                        Idle = true;
                        IsPlayerHugging = false;
                        AccelerationSpeed = orig_acceleration_speed;
                        MoveSpeed = orig_move_speed;
                    }
                    else if (Input.anyKey)
                    {
                        anim.SetBool("idle", false);
                        if (!IsPlayerHugging)
                        {
                            anim.SetBool("isboyhugging", false);
                            anim.SetBool("isgirlhugging", false);
                        }
                        anim.SetBool("girlidle", false);
                        Idle = false;
                    }
                    if (Input.GetKey(KeyCode.Joystick2Button3))
                    {
                        anim.SetInteger("direction", 5);
                        is_moving = 1;
                        float temp_x = MoveSpeed.x;
                        temp_x += AccelerationSpeed.x;
                        MoveSpeed.x = temp_x;
                        float temp_z = MoveSpeed.y;
                        temp_z += AccelerationSpeed.y;
                        MoveSpeed.y = temp_z;
                        transform.Translate(-MoveSpeed.x * Time.deltaTime, 0.0f, MoveSpeed.y * Time.deltaTime);
                    }
                    if (Input.GetKey(KeyCode.Joystick2Button1))
                    {
                        anim.SetInteger("direction", 6);
                        is_moving = 2;
                        float temp_x = MoveSpeed.x;
                        temp_x += AccelerationSpeed.x;
                        MoveSpeed.x = temp_x;
                        float temp_z = MoveSpeed.y;
                        temp_z += AccelerationSpeed.y;
                        MoveSpeed.y = temp_z;
                        transform.Translate(MoveSpeed.x * Time.deltaTime, 0.0f, MoveSpeed.y * Time.deltaTime);
                    }
                    if (Input.GetKey(KeyCode.Joystick2Button3) && Input.GetKey(KeyCode.Joystick2Button1) && !IsPlayerHugging)
                    {
                        anim.SetInteger("direction", 2);
                        is_moving = 3;
                        float temp_z = MoveSpeed.z;
                        temp_z += AccelerationSpeed.z;
                        MoveSpeed.z = temp_z;
                        transform.Translate(0.0f, 0.0f, MoveSpeed.z * Time.deltaTime);
                    }
                    if (MoveSpeed.x >= TargetSpeed.x)
                    {
                        MoveSpeed.x = TargetSpeed.x;
                    }
                    if (MoveSpeed.y >= TargetSpeed.y)
                    {
                        MoveSpeed.y = TargetSpeed.y;
                    }
                    if (MoveSpeed.z >= TargetSpeed.z)
                    {
                        MoveSpeed.z = TargetSpeed.z;
                    }

                    //if (Input.GetKey(KeyCode.Joystick1Button0) && Input.GetKey(KeyCode.Joystick1Button1) && !PlayerOneControl && anim.GetBool("girlscared"))
                    //{
                    //    PlayerOneControl = true;
                    //    anim.SetBool("girlscared", false);
                    //    anim.SetInteger("direction", 2);
                    //}
                }
                if (Input.GetKey(KeyCode.Joystick2Button3) && Input.GetKey(KeyCode.Joystick2Button1) && Input.GetKey(KeyCode.Joystick1Button3) && Input.GetKey(KeyCode.Joystick1Button1) && !IsPlayerHugging)
                {
                    IsPlayerHugging = true;
                    //Moveable = false;
                    anim.SetBool("isgirlhugging", true);
                }
            }
        }
    }
    public void SetMoveable(bool value)
    {
        Moveable = value;
    }
    public int IsMovingDirection()
    {
        return is_moving;
    }
    public bool IsPlayerIdle()
    {
        return Idle;
    }
    public bool IsHugging()
    {
        return IsPlayerHugging;
    }
    public void SetMovingFear(bool val)
    {
        movingForward = val;
    }
    private void FearCheck()
    {
        checkFear -= Time.deltaTime;
        if (checkFear <= 0f)
        {
            if(!this.GetComponent<AudioSource>().isPlaying)
                playScared = true;
            SetMoveable(false);
            if (!PlayerOneControl)
            {
                anim.SetBool("girlscared", true);
            }
            else
            {
                anim.SetBool("boyscared", true);
            }
            ScaredSound();
            //switch fear anim
        }
        if (DebugControls)
        {
            if (PlayerOneControl)
            {
                if (Input.GetKey(KeyCode.A) && anim.GetBool("boyscared") || Input.GetKey(KeyCode.D) && anim.GetBool("boyscared"))
                {
                    PlayerOneControl = false;
                    checkFear = origTimer;
                    SetMoveable(true);
                    anim.SetBool("boyscared", false);
                    anim.SetInteger("direction", 1);
                }
            }
            if (!PlayerOneControl)
                if (Input.GetKey(KeyCode.LeftArrow) && anim.GetBool("girlscared") || Input.GetKey(KeyCode.RightArrow) && anim.GetBool("girlscared"))
                {
                    PlayerOneControl = true;
                    checkFear = origTimer;
                    SetMoveable(true);
                    anim.SetBool("girlscared", false);
                    anim.SetInteger("direction", 2);
                }
        }
        if (!DebugControls)
        {
            if (PlayerOneControl)
            {
                if (Input.GetKey(KeyCode.Joystick2Button3) && anim.GetBool("boyscared") || Input.GetKey(KeyCode.Joystick2Button1) && anim.GetBool("boyscared"))
                {
                    PlayerOneControl = false;
                    checkFear = origTimer;
                    SetMoveable(true);
                    anim.SetBool("boyscared", false);
                    anim.SetInteger("direction", 1);
                }
            }
            if (!PlayerOneControl)
                if (Input.GetKey(KeyCode.Joystick1Button3) && anim.GetBool("girlscared") || Input.GetKey(KeyCode.Joystick1Button1) && anim.GetBool("girlscared"))
                {
                    PlayerOneControl = true;
                    checkFear = origTimer;
                    SetMoveable(true);
                    anim.SetBool("girlscared", false);
                    anim.SetInteger("direction", 2);
                }
        }
    }
    void OnTriggerEnter(Collider col)
    {
        if(!GetComponent<AudioSource>().isPlaying)
        {
            playGrass = true;
        }
            if (col.gameObject.tag == "Grass")
        {           
            PlaySoundGrass();
        }
                
    }
    private void PlaySoundGrass()
    {
        if (Input.anyKey)
        {
            if (GameObject.FindGameObjectWithTag("Bakground_Music"))
                GameObject.FindGameObjectWithTag("Bakground_Music").GetComponent<AudioSource>().volume = 0.5f;
            if (playGrass)
            {              
                this.GetComponent<AudioSource>().PlayOneShot(Grass, 1f);
                playGrass = false;
            }
        }
        else
            this.GetComponent<AudioSource>().Stop();
    }
    private void ScaredSound()
    {
        if (anim.GetBool("girlscared"))
        {
            if (GameObject.FindGameObjectWithTag("Bakground_Music"))
                GameObject.FindGameObjectWithTag("Bakground_Music").GetComponent<AudioSource>().volume = 0.1f;
            //this.GetComponent<AudioSource>().clip = boyScared;     
            if (playScared)
            {
                this.GetComponent<AudioSource>().PlayOneShot(boyScared, 1f);
                playScared = false;
            }
        }
        if (anim.GetBool("boyscared"))
        {
            if (GameObject.FindGameObjectWithTag("Bakground_Music"))
                GameObject.FindGameObjectWithTag("Bakground_Music").GetComponent<AudioSource>().volume = 0.1f;
            //this.GetComponent<AudioSource>().clip = girlScared;
            if (playScared)
            {
                this.GetComponent<AudioSource>().PlayOneShot(girlScared, 1f);
                playScared = false;
            }
        }
    }
}
