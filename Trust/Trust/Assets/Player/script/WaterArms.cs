﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class WaterArms : MonoBehaviour {

    public bool Activated;
    private bool BubbleBool = false;
    public AudioClip BubbleHands;
    AudioSource Audio;
    private GameObject Arms;
    private Animator anim;
    

  //  private Vector3 OriginalPosition;

    public float Speed;
    private float TimeLength;

	// Use this for initialization
	void Start () {
        Audio = GetComponent<AudioSource>();
        anim = GetComponent<Animator>();
        Arms = this.gameObject;

        Arms.GetComponent<Renderer>().enabled = false;


       // OriginalPosition = transform.position;




    }
	
	// Update is called once per frame
	void Update () {


       
        if (Activated == true)
        {
           TimeLength += Time.deltaTime;
            
            Debug.Log("PlayBubbles");
            if (anim.isInitialized)
            {
                BubbleBool = true;
            }
            if (BubbleBool)
            {
                BubblingHands();
            }
            
        }

       if (TimeLength >= 3.2f)
        {
            SceneManager.LoadScene("CrossingStreamEvent");
        }

        if(Activated == false)
        {
            TimeLength = 0.0f;
        }

	
	}

    public void IsActive(bool p_bValue)
    {
        Activated = p_bValue;
    }
    private bool Active()
    {
        return Activated;
       
    }
    
    public void BubblingHands()
    {
        if (BubbleBool)
        { 
        Audio.PlayOneShot(BubbleHands, 1);
        BubbleBool = false;
        }
    }

 
}
