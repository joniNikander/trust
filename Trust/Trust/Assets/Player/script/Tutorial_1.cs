﻿using UnityEngine;
using System.Collections;

public class Tutorial_1 : MonoBehaviour {

    public GameObject Player;
    private GameObject[] Hands;

 
  

    // Use this for initialization
    void Start () {

        // Player = GameObject.FindGameObjectWithTag("Player_One");
        GetComponent<Tutorial_1>().enabled = true;

        Hands = GameObject.FindGameObjectsWithTag("Hands");

  

    }
	
	// Update is called once per frame
	void Update () {

        if(Input.GetKey(KeyCode.LeftArrow) && Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.Joystick1Button3) && Input.GetKey(KeyCode.Joystick1Button1))
        {
            Player.GetComponent<movement_player_one>().enabled = true;
            Player.GetComponent<Animator>().SetInteger("tut", 0);
            Player.GetComponent<Animator>().SetInteger("direction", 1);

            Hands[0].GetComponent<Renderer>().enabled = false;
            Hands[1].GetComponent<Renderer>().enabled = false;



        }

    
  

        if (Player.GetComponent<Animator>().GetInteger("tut") == 1)
            Player.GetComponent<movement_player_one>().enabled = false;
        

    }

    void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.tag == "Player_One")
        {
            Player.GetComponent<Animator>().SetInteger("tut", 1);
            Player.GetComponent<Animator>().SetInteger("direction", 0);
        }
    }
    void OnTriggerExit(Collider col)
    {
   
        GetComponentInChildren<Light>().enabled = false;
        if (col.gameObject.tag == "Player_One")
            GetComponent<Tutorial_1>().enabled = false;
   
    }
}
