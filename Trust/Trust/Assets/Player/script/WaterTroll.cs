﻿using UnityEngine;
using System.Collections;

public class WaterTroll : MonoBehaviour {

    private GameObject Troll;
    public float Speed;

    public bool Activated;
    private bool Peek;
    private bool ReturnPos;

	// Use this for initialization
	void Start () {

        Activated = false;
        Peek = true;
        ReturnPos = false;

        Troll = this.gameObject;

        
	
	}
	
	// Update is called once per frame
	void Update () {

        float step = Speed * Time.deltaTime;
        if (Activated == true)
        {
            Troll.SetActive(true);

            Vector3 PeekPosition = new Vector3(transform.position.x, 2.2f, transform.position.z);

            if (Peek == true)
            {
                transform.position = Vector3.MoveTowards(transform.position, PeekPosition, step);
            }

            if (transform.position == PeekPosition)
            {
                Peek = false;
                ReturnPos = true;
                
                
            }
            Vector3 OriginalPos = new Vector3(transform.position.x, -4.2f, transform.position.z);
            if (ReturnPos == true)
            {
                
                transform.position = Vector3.MoveTowards(transform.position, OriginalPos, step);
            }
            if(transform.position == OriginalPos)
            {
                this.GetComponent<Renderer>().enabled = false;
            }
        }

    }

    public void IsActive(bool p_bvalue)
    {
        Activated = p_bvalue;
    }
   public bool Active()
    {
        return Activated;
    }
}
