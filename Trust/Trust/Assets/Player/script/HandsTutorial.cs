﻿using UnityEngine;
using System.Collections;

public class HandsTutorial : MonoBehaviour {
    public bool leftHand;
    public bool rightHand;
    public float fadeSpeed;
    private SpriteRenderer leftHandMaterial;
    private SpriteRenderer rightHandMaterial;
    private Color left;
    private Color right;
    private SpriteRenderer girlleftHandMaterial;
    private SpriteRenderer girlrightHandMaterial;
    private Color girlleft;
    private Color girlright;
    public bool fadeoutleft = false;
    public bool fadeoutright = false;
	// Use this for initialization
	void Start () {
        leftHandMaterial = GameObject.FindGameObjectWithTag("HandLeft").GetComponent<SpriteRenderer>();
        rightHandMaterial = GameObject.FindGameObjectWithTag("HandRight").GetComponent<SpriteRenderer>();
        left = GameObject.Find("HandLeft").GetComponent<SpriteRenderer>().color;
        right = GameObject.Find("HandRight").GetComponent<SpriteRenderer>().color;
        leftHandMaterial.color = Color.clear;
        rightHandMaterial.color = Color.clear;

        girlleftHandMaterial = GameObject.FindGameObjectWithTag("GirlHandLeft").GetComponent<SpriteRenderer>();
        girlrightHandMaterial = GameObject.FindGameObjectWithTag("GirlHandRight").GetComponent<SpriteRenderer>();
        girlleft = GameObject.Find("GirlHandLeft").GetComponent<SpriteRenderer>().color;
        girlright = GameObject.Find("GirlHandRight").GetComponent<SpriteRenderer>().color;
        girlleftHandMaterial.color = Color.clear;
        girlrightHandMaterial.color = Color.clear;
    }
	
	// Update is called once per frame
	void Update () {
        if(leftHand)
        {
            if(leftHandMaterial.color.a != 1f)
                FadeInLeft();
            if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.Joystick1Button0))
            {
                leftHand = false;
                fadeoutleft = true;
            }
        }
        if(rightHand)
        {
            if(rightHandMaterial.color.a != 1f)
                FadeInRight();
            if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.Joystick1Button1))
            {
                rightHand = false;
                fadeoutright = true;
            }
        }
	    if(fadeoutleft)
            FadeLeft();
        if (fadeoutright)
            FadeRight();
        if (leftHandMaterial.color.a <= 0.05f)
        {
            fadeoutleft = false;
            leftHandMaterial.color = Color.clear;
        }
        if (rightHandMaterial.color.a <= 0.05f)
        {
            fadeoutright = false;
            rightHandMaterial.color = Color.clear;
        }
        Tutorial();
    }
    private void FadeLeft()
    {
            leftHandMaterial.color = Color.Lerp(leftHandMaterial.color, Color.clear, fadeSpeed * Time.deltaTime);
    }
    private void FadeRight()
    {
        rightHandMaterial.color = Color.Lerp(rightHandMaterial.color, Color.clear, fadeSpeed * Time.deltaTime);
    }
    private void FadeInLeft()
    {
        leftHandMaterial.color = Color.Lerp(leftHandMaterial.color, left, fadeSpeed * Time.deltaTime);
    }
    private void FadeInRight()
    {
        rightHandMaterial.color = Color.Lerp(rightHandMaterial.color, right, fadeSpeed * Time.deltaTime);
    }
    private void FadeGirlLeft()
    {
        girlleftHandMaterial.color = Color.Lerp(girlleftHandMaterial.color, girlleft, fadeSpeed * Time.deltaTime);
    }
    private void FadeGirlRight()
    {
        girlrightHandMaterial.color = Color.Lerp(girlrightHandMaterial.color, girlright, fadeSpeed * Time.deltaTime);
    }
    private void FadeOutGirlLeft()
    {
        girlleftHandMaterial.color = Color.Lerp(girlleftHandMaterial.color, Color.clear, fadeSpeed * Time.deltaTime);
    }
    private void FadeOutGirlRight()
    {
        girlrightHandMaterial.color = Color.Lerp(girlrightHandMaterial.color, Color.clear, fadeSpeed * Time.deltaTime);
    }
    private void Tutorial()
    {

        if (GetComponent<Animator>().GetInteger("tut") == 2)
        {
            SetLeftHand(true);
        }
        if (GetComponent<Animator>().GetInteger("tut") == 3)
        {
            SetLeftHand(false);
            SetRightHand(true);
        }
        if (GetComponent<Animator>().GetInteger("tut") == 0)
            SetRightHand(false);

        if(GetComponent<Animator>().GetInteger("tut") == 4)
        {
            SetLeftHand(true);
            SetRightHand(true);
            FadeGirlLeft();
            FadeGirlRight();
        }
        if (GetComponent<Animator>().GetInteger("tut") != 4)
        {
            FadeOutGirlLeft();
            FadeOutGirlRight();
            FadeLeft();
            FadeRight();
        }
    }
   public void SetLeftHand(bool val)
    {
        leftHand = val;
    }
    public void SetRightHand(bool val)
    {
        rightHand = val;
    }
}
