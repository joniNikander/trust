﻿using UnityEngine;
using System.Collections;

public class StonePosition2 : MonoBehaviour {

    private float Puzzle;
    public Vector2 randomValues;
    private GameObject TargetStone;

    // Use this for initialization
    void Start()
    {

        Puzzle = Random.Range(randomValues.x, randomValues.y);

        TargetStone = this.gameObject;

        TargetStone.transform.position = new Vector3(TargetStone.transform.position.x + Puzzle, TargetStone.transform.position.y, TargetStone.transform.position.z);

    }

    // Update is called once per frame
    void Update () {
	
	}
}
