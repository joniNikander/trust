﻿using UnityEngine;
using System.Collections;

public class SpiderMovement : MonoBehaviour {
    public AudioClip SpiderSound;
    AudioSource Audio;
    private bool playSpiderSound = false;


    private Transform Spider;
    private GameObject Player;
    public float Speed;
    public Transform target;
    public Vector3 offset = new Vector3(0f, 7.5f, 0f);
    private bool notDescending;

    // Use this for initialization
    void Start ()
    {
        notDescending = false;
        Audio = GetComponent<AudioSource>();

        

        this.GetComponent<Animator>().enabled = false;
        this.GetComponent<SpriteRenderer>().enabled = false;


        Spider = this.gameObject.transform;
        Player = GameObject.FindGameObjectWithTag("Player_One");
      

	}
	
	// Update is called once per frame
	void Update () {

        if (playSpiderSound)
        {
            SpiderSoundYo();
        }

       float step = Speed * Time.deltaTime;

        Vector3 Descend = new Vector3(Spider.position.x,  18.0f, Spider.position.z);

        if (Player.GetComponent<movement_player_one>().IsHugging() == true)
        {
            notDescending = true;
            Spider.localPosition = Vector3.MoveTowards(transform.position, Descend, step);
            playSpiderSound = false;
        }
        else
            notDescending = false;
        

        if (transform.position.y >= 18)
        {
            Player.GetComponent<Spider>().IsActive(false);
            transform.position = target.position + offset;
            this.GetComponent<Animator>().enabled = false;
            this.GetComponent<SpriteRenderer>().enabled = false;
            this.GetComponent<Animator>().Play("Spider", -1, 0f);
            this.enabled = false;
            
        }
        if (Player.GetComponent<Spider>().Active() == true)
        {
            playSpiderSound = true;
            this.GetComponent<Animator>().enabled = true;
            this.GetComponent<SpriteRenderer>().enabled = true;
            Player.GetComponent<Fear>().IncreaseFear();
        }
	
	}
    private void LateUpdate()
    {
        if(!notDescending)
            transform.position = target.position + offset;
    }

    void SpiderSoundYo()
    {
        if (playSpiderSound)
        {
           
                Audio.PlayOneShot(SpiderSound, 1);
                playSpiderSound = false;
          
            if (Audio.isPlaying)
            {
                playSpiderSound = false;
            }

        }
        
            
    }
}
