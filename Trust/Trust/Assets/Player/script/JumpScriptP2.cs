﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class JumpScriptP2 : MonoBehaviour
{
    AudioSource Audio;
    public AudioClip jumpGirl;
    private bool playJumpSound = false;
    private bool playBubbleHands = false;

    public bool Moveable;
    public bool Debugcontrolls;
    public bool EventFinished;
    private GameObject[] Arms;
    private GameObject PlayerOne;
    private GameObject Bubbles;
    public Transform EventStart;
    public Transform Target;
    public Transform Target1;
    public Transform Target2;
    public Transform Target3;
    public Transform Target4;
    public Transform Target5;
    public Transform Finish;


    public Transform FakeTarget;
    public Transform FakeTarget1;
    public Transform FakeTarget2;
    public Transform FakeTarget3;

    public Sprite JumpLeft;
    public Sprite JumpRight;
    public Sprite Idle;

    private bool Jumpable;


    private bool Stone;
    private bool Stone1;
    private bool Stone2;
    private bool Stone3;
    private bool Stone4;
    private bool Stone5;
    private bool Stone6;
    private bool Goal;

    private bool FakeStone;
    private bool FakeStone1;
    private bool FakeStone2;
    private bool FakeStone3;


    private bool Jump;
    private bool Jump1;
    private bool Jump2;
    private bool Jump3;
    private bool Jump4;
    private bool Jump5;
    private bool Jump6;


    private bool FakeJump;
    private bool FakeJump1;
    private bool FakeJump2;
    private bool FakeJump3;

    private bool TimerStart;

    public float Speed;
    public float Timer;
    private float Original_Timer;



    // Use this for initialization
    void Start()
    {
        Audio = this.GetComponent<AudioSource>();

        Original_Timer = Timer;
        Bubbles = GameObject.FindGameObjectWithTag("Bubbles");
        Arms = GameObject.FindGameObjectsWithTag("Arm");
        PlayerOne = GameObject.FindGameObjectWithTag("PlayerOne");
        Moveable = true;
        EventFinished = false;
        Stone = false;
        Stone1 = false;
        Stone2 = false;
        Stone3 = false;
        Stone4 = false;
        Stone5 = false;

        FakeStone = false;
        FakeStone1 = false;
        FakeStone2 = false;
        FakeStone3 = false;


        Jump = false;
        Jump1 = false;
        Jump2 = false;
        Jump3 = false;
        Jump4 = false;
        Jump5 = false;
        Jump6 = false;

        FakeJump = false;
        FakeJump1 = false;
        FakeJump2 = false;
        FakeJump3 = false;

        TimerStart = false;

    }

    // Update is called once per frame
    void Update()
    {


        if (Jumpable == false)
        {
            Debugcontrolls = true;
        }
        else
        {
            Debugcontrolls = false;
        }

        if (playJumpSound)
        {
            PlayJumpSound();
        }

        //------------------------------------------
        if (Debugcontrolls == false)
        {
            float step = Speed * Time.deltaTime;

            if (TimerStart == true)
            {
                Timer -= Time.deltaTime;

                if (Timer <= 0)
                {
                    Arms[0].GetComponent<WaterArms>().IsActive(true);
                    Arms[1].GetComponent<WaterArms>().IsActive(true);
                    Arms[0].GetComponent<Renderer>().enabled = true;
                    Arms[1].GetComponent<Renderer>().enabled = true;
                    Arms[0].GetComponent<Animator>().enabled = true;
                    Arms[1].GetComponent<Animator>().enabled = true;
                  

                }
                else if (Timer > 0)
                {
                    Arms[0].GetComponent<WaterArms>().IsActive(false);
                    Arms[1].GetComponent<WaterArms>().IsActive(false);
                    Arms[0].GetComponent<Renderer>().enabled = false;
                    Arms[1].GetComponent<Renderer>().enabled = false;
                    Arms[0].GetComponent<Animator>().enabled = false;
                    Arms[1].GetComponent<Animator>().enabled = false;
                    Arms[0].GetComponent<Animator>().Play("WaterArm", 0, 0f);
                    Arms[1].GetComponent<Animator>().Play("WaterArm", 0, 0f);
                    

                }


            }


            if (Target.position.x > EventStart.position.x)
            {

                if (Stone == true && Input.GetKey(KeyCode.Joystick2Button1))
                {

                    
                    Jump = true;
                    Timer = Original_Timer;
                    Arms[0].GetComponent<WaterArms>().IsActive(false);
                    Arms[1].GetComponent<WaterArms>().IsActive(false);


                }
            }
            if (Target.position.x < EventStart.position.x)
            {

                if (Stone == true && Input.GetKey(KeyCode.Joystick2Button3))
                {
                   
                    Arms[0].GetComponent<WaterArms>().IsActive(false);
                    Arms[1].GetComponent<WaterArms>().IsActive(false);
                    Jump = true;
                    Timer = Original_Timer;

                }
            }


            if (Jump == true)
            {
                if (!Audio.isPlaying)
             
                    GetComponent<SpriteRenderer>().sprite = JumpLeft;
                Vector3 targetPos = new Vector3(Target.position.x, Target.position.y + 3.5f, Target.position.z);
                float y = targetPos.y + 10;
                Vector3 targetJumpPos = new Vector3(Target.position.x, y, Target.position.z);
                if (transform.position.z >= targetPos.z / 2)
                    transform.position = Vector3.MoveTowards(transform.position, targetPos, step);
                else
                    transform.position = Vector3.MoveTowards(transform.position, targetJumpPos, step);


            }
            else
                GetComponent<SpriteRenderer>().sprite = Idle;
            if (FakeTarget.position.x > Target.position.x)
            {


                if (FakeStone == true && Input.GetKey(KeyCode.Joystick2Button1))
                {
                
                    Stone1 = false;
                    FakeJump = true;
                    Timer = Original_Timer;

                }
            }
            if (FakeTarget.position.x < Target.position.x)
            {
                if (FakeStone == true && Input.GetKey(KeyCode.Joystick2Button3))
                {
                 
                    Stone1 = false;
                    FakeJump = true;
                    Timer = Original_Timer;

                }
            }

            else if (FakeJump == true)
            {
                GetComponent<SpriteRenderer>().sprite = JumpRight;
                Vector3 FaketargetPos = new Vector3(FakeTarget.position.x, FakeTarget.position.y + 3.5f, FakeTarget.position.z);
                float y = FaketargetPos.y + 10;
                Vector3 targetJumpPos = new Vector3(FakeTarget.position.x, y, FakeTarget.position.z);
                if (transform.position.z >= FaketargetPos.z * 0.9)
                    transform.position = Vector3.MoveTowards(transform.position, FaketargetPos, step);
                else
                    transform.position = Vector3.MoveTowards(transform.position, targetJumpPos, step);
            }

            if (Target1.position.x > Target.position.x)
            {
                if (Stone1 == true && Input.GetKey(KeyCode.Joystick2Button1))
                {
                 
                    FakeStone = false;
                    Jump1 = true;
                    Timer = Original_Timer;
                    Arms[0].GetComponent<WaterArms>().IsActive(false);
                    Arms[1].GetComponent<WaterArms>().IsActive(false);
                    Arms[0].GetComponent<Renderer>().enabled = false;
                    Arms[1].GetComponent<Renderer>().enabled = false;
                   
                }
            }
            if (Target1.position.x < Target.position.x)
            {
                if (Stone1 == true && Input.GetKey(KeyCode.Joystick2Button3))
                {
                
                    FakeStone = false;
                    Jump1 = true;
                    Timer = Original_Timer;
                    Arms[0].GetComponent<WaterArms>().IsActive(false);
                    Arms[1].GetComponent<WaterArms>().IsActive(false);
                    Arms[0].GetComponent<Renderer>().enabled = false;
                    Arms[1].GetComponent<Renderer>().enabled = false;
               
                }
            }

            if (Jump1 == true)
            {
                GetComponent<SpriteRenderer>().sprite = JumpLeft;
                Vector3 targetPos1 = new Vector3(Target1.position.x, Target1.position.y + 1.2f, Target1.position.z);
                float y = targetPos1.y + 10;
                Vector3 targetJumpPos = new Vector3(Target1.position.x, y, Target1.position.z);
                if (transform.position.z >= targetPos1.z * 0.9)
                    transform.position = Vector3.MoveTowards(transform.position, targetPos1, step);
                else
                    transform.position = Vector3.MoveTowards(transform.position, targetJumpPos, step);
            }

            if (FakeTarget1.position.x > Target1.position.x)
            {
                if (FakeStone1 == true && Input.GetKey(KeyCode.Joystick2Button1))
                {
                
                    Stone2 = false;
                    FakeJump1 = true;
                    Timer = Original_Timer;

                }
            }
            if (FakeTarget1.position.x < Target1.position.x)
            {
                if (FakeStone1 == true && Input.GetKey(KeyCode.Joystick2Button3))
                {
                 
                    Stone2 = false;
                    FakeJump1 = true;
                    Timer = Original_Timer;

                }
            }

            if (FakeJump1 == true)
            {
                GetComponent<SpriteRenderer>().sprite = JumpLeft;
                Vector3 FaketargetPos1 = new Vector3(FakeTarget1.position.x, FakeTarget1.position.y + 1.2f, FakeTarget1.position.z);
                float y = FaketargetPos1.y + 10;
                Vector3 targetJumpPos = new Vector3(FakeTarget1.position.x, y, FakeTarget1.position.z);
                if (transform.position.z >= FaketargetPos1.z * 0.9)
                    transform.position = Vector3.MoveTowards(transform.position, FaketargetPos1, step);
                else
                    transform.position = Vector3.MoveTowards(transform.position, targetJumpPos, step);
            }

            if (Target2.position.x > Target1.position.x)
            {
                if (Stone2 == true && Input.GetKey(KeyCode.Joystick2Button1))
                {
                
                    FakeJump1 = false;
                    Jump2 = true;
                    Timer = Original_Timer;
                    Arms[0].GetComponent<WaterArms>().IsActive(false);
                    Arms[1].GetComponent<WaterArms>().IsActive(false);
                    Arms[0].GetComponent<Renderer>().enabled = false;
                    Arms[1].GetComponent<Renderer>().enabled = false;
                 
                }
            }
            if (Target2.position.x < Target1.position.x)
            {
                if (Stone2 == true && Input.GetKey(KeyCode.Joystick2Button3))
                {
                
                    FakeJump1 = false;
                    Jump2 = true;
                    Timer = Original_Timer;
                    Arms[0].GetComponent<WaterArms>().IsActive(false);
                    Arms[1].GetComponent<WaterArms>().IsActive(false);
                    Arms[0].GetComponent<Renderer>().enabled = false;
                    Arms[1].GetComponent<Renderer>().enabled = false;
               
                }
            }
            if (Jump2 == true)
            {
                GetComponent<SpriteRenderer>().sprite = JumpRight;
                Vector3 targetPos2 = new Vector3(Target2.position.x, Target2.position.y + 1.2f, Target2.position.z);
                float y = targetPos2.y + 10;
                Vector3 targetJumpPos = new Vector3(Target2.position.x, y, Target2.position.z);
                if (transform.position.z >= targetPos2.z * 0.9)
                    transform.position = Vector3.MoveTowards(transform.position, targetPos2, step);
                else
                    transform.position = Vector3.MoveTowards(transform.position, targetJumpPos, step);
            }

            if (FakeTarget2.position.x > Target2.position.x)
            {
                if (FakeStone2 == true && Input.GetKey(KeyCode.Joystick2Button1))
                {
           
                    Stone3 = false;
                    FakeJump2 = true;
                    Timer = Original_Timer;

                }
            }
            if (FakeTarget2.position.x < Target2.position.x)
            {
                if (FakeStone2 == true && Input.GetKey(KeyCode.Joystick2Button3))
                {
             
                    Stone3 = false;
                    FakeJump2 = true;
                    Timer = Original_Timer;

                }
            }

            if (FakeJump2 == true)
            {
                GetComponent<SpriteRenderer>().sprite = JumpLeft;
                Vector3 FaketargetPos2 = new Vector3(FakeTarget2.position.x, FakeTarget2.position.y + 1.2f, FakeTarget2.position.z);
                float y = FaketargetPos2.y + 10;
                Vector3 targetJumpPos = new Vector3(FakeTarget2.position.x, y, FakeTarget2.position.z);
                if (transform.position.z >= FaketargetPos2.z * 0.9)
                    transform.position = Vector3.MoveTowards(transform.position, FaketargetPos2, step);
                else
                    transform.position = Vector3.MoveTowards(transform.position, targetJumpPos, step);
            }

            if (Target3.position.x > Target2.position.x)
            {
                if (Stone3 == true && Input.GetKey(KeyCode.Joystick2Button1))
                {
           
                    FakeStone2 = false;
                    Jump3 = true;
                    Timer = Original_Timer;
                    Arms[0].GetComponent<WaterArms>().IsActive(false);
                    Arms[1].GetComponent<WaterArms>().IsActive(false);
                    Arms[0].GetComponent<Renderer>().enabled = false;
                    Arms[1].GetComponent<Renderer>().enabled = false;
                   
                }
            }
            if (Target3.position.x < Target2.position.x)
            {
                if (Stone3 == true && Input.GetKey(KeyCode.Joystick2Button3))
                {
        
                    FakeStone2 = false;
                    Jump3 = true;
                    Timer = Original_Timer;
                    Arms[0].GetComponent<WaterArms>().IsActive(false);
                    Arms[1].GetComponent<WaterArms>().IsActive(false);
                    Arms[0].GetComponent<Renderer>().enabled = false;
                    Arms[1].GetComponent<Renderer>().enabled = false;
                 
                }
            }
            if (Jump3 == true)
            {
                GetComponent<SpriteRenderer>().sprite = JumpRight;
                Vector3 targetPos3 = new Vector3(Target3.position.x, Target3.position.y + 2.7f, Target3.position.z);
                float y = targetPos3.y + 10;
                Vector3 targetJumpPos = new Vector3(Target3.position.x, y, Target3.position.z);
                if (transform.position.z >= targetPos3.z * 0.9)
                    transform.position = Vector3.MoveTowards(transform.position, targetPos3, step);
                else
                    transform.position = Vector3.MoveTowards(transform.position, targetJumpPos, step);
            }

            if (FakeTarget3.position.x > Target3.position.x)
            {
                if (FakeStone3 == true && Input.GetKey(KeyCode.Joystick2Button1))
                {
             
                    Stone4 = false;
                    FakeJump3 = true;
                    Timer = Original_Timer;

                }
            }

            if (FakeTarget3.position.x < Target3.position.x)
            {
                if (FakeStone3 == true && Input.GetKey(KeyCode.Joystick2Button3))
                {
            
                    Stone4 = false;
                    FakeJump3 = true;
                    Timer = Original_Timer;

                }
            }

            if (FakeJump3 == true)
            {
                GetComponent<SpriteRenderer>().sprite = JumpRight;
                Vector3 FaketargetPos3 = new Vector3(FakeTarget3.position.x, FakeTarget3.position.y + 2.5f, FakeTarget3.position.z);
                float y = FaketargetPos3.y + 10;
                Vector3 targetJumpPos = new Vector3(FakeTarget3.position.x, y, FakeTarget3.position.z);
                if (transform.position.z >= FaketargetPos3.z * 0.9)
                    transform.position = Vector3.MoveTowards(transform.position, FaketargetPos3, step);
                else
                    transform.position = Vector3.MoveTowards(transform.position, targetJumpPos, step);
            }
            if (Target4.position.x > Target3.position.x)
            {
                if (Stone4 == true && Input.GetKey(KeyCode.Joystick2Button1))
                {
            
                    FakeStone3 = false;
                    Jump4 = true;
                    Timer = Original_Timer;
                    Arms[0].GetComponent<WaterArms>().IsActive(false);
                    Arms[1].GetComponent<WaterArms>().IsActive(false);
                    Arms[0].GetComponent<Renderer>().enabled = false;
                    Arms[1].GetComponent<Renderer>().enabled = false;
                 
                }
            }

            if (Target4.position.x < Target3.position.x)
            {
                if (Stone4 == true && Input.GetKey(KeyCode.Joystick2Button3))
                {
             
                    FakeStone3 = false;
                    Jump4 = true;
                    Timer = Original_Timer;
                    Arms[0].GetComponent<WaterArms>().IsActive(false);
                    Arms[1].GetComponent<WaterArms>().IsActive(false);
                    Arms[0].GetComponent<Renderer>().enabled = false;
                    Arms[1].GetComponent<Renderer>().enabled = false;
             
                }
            }
            if (Jump4 == true)
            {
                GetComponent<SpriteRenderer>().sprite = JumpLeft;
                Vector3 targetPos4 = new Vector3(Target4.position.x, Target4.position.y + 2.5f, Target4.position.z);
                float y = targetPos4.y + 10;
                Vector3 targetJumpPos = new Vector3(Target4.position.x, y, Target4.position.z);
                if (transform.position.z >= targetPos4.z * 0.9)
                    transform.position = Vector3.MoveTowards(transform.position, targetPos4, step);
                else
                    transform.position = Vector3.MoveTowards(transform.position, targetJumpPos, step);
            }

            if (Target5.position.x > Target4.position.x)
            {
                if (Stone5 == true && Input.GetKey(KeyCode.Joystick2Button1))
                {
               
                    Jump5 = true;
                    Timer = Original_Timer;
                    Arms[0].GetComponent<WaterArms>().IsActive(false);
                    Arms[1].GetComponent<WaterArms>().IsActive(false);
                    Arms[0].GetComponent<Renderer>().enabled = false;
                    Arms[1].GetComponent<Renderer>().enabled = false;
                
                }
            }

            if (Target5.position.x < Target4.position.x)
            {
                if (Stone5 == true && Input.GetKey(KeyCode.Joystick2Button3))
                {
           
                    Jump5 = true;
                    Timer = Original_Timer;
                    Arms[0].GetComponent<WaterArms>().IsActive(false);
                    Arms[1].GetComponent<WaterArms>().IsActive(false);
                    Arms[0].GetComponent<Renderer>().enabled = false;
                    Arms[1].GetComponent<Renderer>().enabled = false;
        
                }
            }

            if (Jump5 == true)
            {
                GetComponent<SpriteRenderer>().sprite = JumpRight;
                Vector3 targetPos5 = new Vector3(Target5.position.x + 3, Target5.position.y, Target5.position.z);
                float y = targetPos5.y + 10;
                Vector3 targetJumpPos = new Vector3(Target5.position.x, y, Target5.position.z);
                if (transform.position.z >= targetPos5.z * 0.96)
                    transform.position = Vector3.MoveTowards(transform.position, targetPos5, step);
                else
                    transform.position = Vector3.MoveTowards(transform.position, targetJumpPos, step);
            }

            if (Finish.position.x > Target5.position.x)
            {
                if (Goal == true && Input.GetKey(KeyCode.Joystick2Button1))
                {
               
                    Jump6 = true;
                    Timer = Original_Timer;
                    Arms[0].GetComponent<WaterArms>().IsActive(false);
                    Arms[1].GetComponent<WaterArms>().IsActive(false);
                    Arms[0].GetComponent<Renderer>().enabled = false;
                    Arms[1].GetComponent<Renderer>().enabled = false;
              
                }
            }

            if (Finish.position.x < Target5.position.x)
            {
                if (Goal == true && Input.GetKey(KeyCode.Joystick2Button3))
                {
                 
                    Jump6 = true;
                    Timer = Original_Timer;
                    Arms[0].GetComponent<WaterArms>().IsActive(false);
                    Arms[1].GetComponent<WaterArms>().IsActive(false);
                    Arms[0].GetComponent<Renderer>().enabled = false;
                    Arms[1].GetComponent<Renderer>().enabled = false;
                   
                }
            }
            if (Jump6 == true)
            {
                GetComponent<SpriteRenderer>().sprite = JumpLeft;
                Vector3 targetFinish = new Vector3(Finish.position.x, Finish.position.y - 2, Finish.position.z);
                float y = targetFinish.y + 10;
                Vector3 targetJumpPos = new Vector3(Finish.position.x, y, Finish.position.z);
                if (transform.position.z >= targetFinish.z * 0.9)
                    transform.position = Vector3.MoveTowards(transform.position, targetFinish, step);
                else
                    transform.position = Vector3.MoveTowards(transform.position, targetJumpPos, step);
            }
        }
        //PlayerTwo JumpScript, Keyboard Controls
        //-----------------------------------------
        if (Debugcontrolls == true)
        {
            float step = Speed * Time.deltaTime;

            if (TimerStart == true)
            {
                Timer -= Time.deltaTime;

                if (Timer <= 0)
                {
                    Arms[0].GetComponent<WaterArms>().IsActive(true);
                    Arms[1].GetComponent<WaterArms>().IsActive(true);
                    Arms[0].GetComponent<Renderer>().enabled = true;
                    Arms[1].GetComponent<Renderer>().enabled = true;
                    Arms[0].GetComponent<Animator>().enabled = true;
                    Arms[1].GetComponent<Animator>().enabled = true;
                    Arms[0].GetComponent<Animator>().enabled = true;
                    Arms[1].GetComponent<Animator>().enabled = true;



                }
                if (Timer > 0)
                {
                    Arms[0].GetComponent<WaterArms>().IsActive(false);
                    Arms[1].GetComponent<WaterArms>().IsActive(false);
                    Arms[0].GetComponent<Renderer>().enabled = false;
                    Arms[1].GetComponent<Renderer>().enabled = false;
                    Arms[0].GetComponent<Animator>().enabled = false;
                    Arms[1].GetComponent<Animator>().enabled = false;
                    Arms[0].GetComponent<Animator>().Play("WaterArm", 0, 0f);
                    Arms[1].GetComponent<Animator>().Play("WaterArm", 0, 0f);
                }
            }


            if (Target.position.x > EventStart.position.x)
            {

                if (Stone == true && Input.GetKeyDown(KeyCode.D))
                {
                 
                    Jump = true;
                    Timer = Original_Timer;
                    Arms[0].GetComponent<WaterArms>().IsActive(false);
                    Arms[1].GetComponent<WaterArms>().IsActive(false);

                }
            }
            if (Target.position.x < EventStart.position.x)
            {

                if (Stone == true && Input.GetKey(KeyCode.A))
                {
               
                    Arms[0].GetComponent<WaterArms>().IsActive(false);
                    Arms[1].GetComponent<WaterArms>().IsActive(false);
                    Jump = true;
                    Timer = Original_Timer;

                }
            }


            if (Jump == true)
            {

                GetComponent<SpriteRenderer>().sprite = JumpLeft;
                Vector3 targetPos = new Vector3(Target.position.x, Target.position.y + 3.5f, Target.position.z);
                float y = targetPos.y + 10;
                Vector3 targetJumpPos = new Vector3(Target.position.x, y, Target.position.z);
                if (transform.position.z >= targetPos.z / 2)
                    transform.position = Vector3.MoveTowards(transform.position, targetPos, step);
                else
                    transform.position = Vector3.MoveTowards(transform.position, targetJumpPos, step);


            }
            else
                GetComponent<SpriteRenderer>().sprite = Idle;
            if (FakeTarget.position.x > Target.position.x)
            {


                if (FakeStone == true && Input.GetKey(KeyCode.D))
                {
             
                    FakeJump = true;
                    Stone1 = false;
                    Timer = Original_Timer;

                }
            }
            if (FakeTarget.position.x < Target.position.x)
            {
                if (FakeStone == true && Input.GetKey(KeyCode.A))
                {
       
                    FakeJump = true;
                    Stone1 = false;
                    Timer = Original_Timer;

                }
            }

            else if (FakeJump == true)
            {

                GetComponent<SpriteRenderer>().sprite = JumpRight;
                Vector3 FaketargetPos = new Vector3(FakeTarget.position.x, FakeTarget.position.y + 3.5f, FakeTarget.position.z);
                float y = FaketargetPos.y + 10;
                Vector3 targetJumpPos = new Vector3(FakeTarget.position.x, y, FakeTarget.position.z);
                if (transform.position.z >= FaketargetPos.z * 0.9)
                    transform.position = Vector3.MoveTowards(transform.position, FaketargetPos, step);
                else
                    transform.position = Vector3.MoveTowards(transform.position, targetJumpPos, step);
            }

            if (Target1.position.x > Target.position.x)
            {
                if (Stone1 == true && Input.GetKey(KeyCode.D))
                {
                  
                    Jump1 = true;
                    FakeStone = false;
                    Timer = Original_Timer;
                    Arms[0].GetComponent<WaterArms>().IsActive(false);
                    Arms[1].GetComponent<WaterArms>().IsActive(false);
                    Arms[0].GetComponent<Renderer>().enabled = false;
                    Arms[1].GetComponent<Renderer>().enabled = false;
                }
            }
            if (Target1.position.x < Target.position.x)
            {
                if (Stone1 == true && Input.GetKey(KeyCode.A))
                {
               
                    Jump1 = true;
                    FakeStone = false;
                    Timer = Original_Timer;
                    Arms[0].GetComponent<WaterArms>().IsActive(false);
                    Arms[1].GetComponent<WaterArms>().IsActive(false);
                    Arms[0].GetComponent<Renderer>().enabled = false;
                    Arms[1].GetComponent<Renderer>().enabled = false;
                }
            }

            if (Jump1 == true)
            {
                FakeJump = false;
                GetComponent<SpriteRenderer>().sprite = JumpLeft;
                Vector3 targetPos1 = new Vector3(Target1.position.x, Target1.position.y + 1.2f, Target1.position.z);
                float y = targetPos1.y + 10;
                Vector3 targetJumpPos = new Vector3(Target1.position.x, y, Target1.position.z);
                if (transform.position.z >= targetPos1.z * 0.9)
                    transform.position = Vector3.MoveTowards(transform.position, targetPos1, step);
                else
                    transform.position = Vector3.MoveTowards(transform.position, targetJumpPos, step);
            }

            if (FakeTarget1.position.x > Target1.position.x)
            {
                if (FakeStone1 == true && Input.GetKey(KeyCode.D))
                {
                
                    FakeJump1 = true;
                    Stone2 = false;
                    Timer = Original_Timer;

                }
            }
            if (FakeTarget1.position.x < Target1.position.x)
            {
                if (FakeStone1 == true && Input.GetKey(KeyCode.A))
                {
                
                    FakeJump1 = true;
                    Stone2 = false;
                    Timer = Original_Timer;

                }
            }

            if (FakeJump1 == true)
            {
                Jump2 = false;
                GetComponent<SpriteRenderer>().sprite = JumpLeft;
                Vector3 FaketargetPos1 = new Vector3(FakeTarget1.position.x, FakeTarget1.position.y + 1.2f, FakeTarget1.position.z);
                float y = FaketargetPos1.y + 10;
                Vector3 targetJumpPos = new Vector3(FakeTarget1.position.x, y, FakeTarget1.position.z);
                if (transform.position.z >= FaketargetPos1.z * 0.9)
                    transform.position = Vector3.MoveTowards(transform.position, FaketargetPos1, step);
                else
                    transform.position = Vector3.MoveTowards(transform.position, targetJumpPos, step);
            }

            if (Target2.position.x > Target1.position.x)
            {
                if (Stone2 == true && Input.GetKey(KeyCode.D))
                {
                
                    Jump2 = true;
                    FakeStone1 = false;
                    Timer = Original_Timer;
                    Arms[0].GetComponent<WaterArms>().IsActive(false);
                    Arms[1].GetComponent<WaterArms>().IsActive(false);
                    Arms[0].GetComponent<Renderer>().enabled = false;
                    Arms[1].GetComponent<Renderer>().enabled = false;
                }
            }
            if (Target2.position.x < Target1.position.x)
            {
                if (Stone2 == true && Input.GetKey(KeyCode.A))
                {
             
                    Jump2 = true;
                    FakeStone1 = false;
                    Timer = Original_Timer;
                    Arms[0].GetComponent<WaterArms>().IsActive(false);
                    Arms[1].GetComponent<WaterArms>().IsActive(false);
                    Arms[0].GetComponent<Renderer>().enabled = false;
                    Arms[1].GetComponent<Renderer>().enabled = false;
                }
            }
            if (Jump2 == true)
            {
                FakeJump1 = false;
                GetComponent<SpriteRenderer>().sprite = JumpRight;
                Vector3 targetPos2 = new Vector3(Target2.position.x, Target2.position.y + 1.2f, Target2.position.z);
                float y = targetPos2.y + 10;
                Vector3 targetJumpPos = new Vector3(Target2.position.x, y, Target2.position.z);
                if (transform.position.z >= targetPos2.z * 0.9)
                    transform.position = Vector3.MoveTowards(transform.position, targetPos2, step);
                else
                    transform.position = Vector3.MoveTowards(transform.position, targetJumpPos, step);
            }

            if (FakeTarget2.position.x > Target2.position.x)
            {
                if (FakeStone2 == true && Input.GetKey(KeyCode.D))
                {
             
                    FakeJump2 = true;
                    Stone3 = false;
                    Timer = Original_Timer;

                }
            }
            if (FakeTarget2.position.x < Target2.position.x)
            {
                if (FakeStone2 == true && Input.GetKey(KeyCode.A))
                {
         
                    FakeJump2 = true;
                    Stone3 = false;
                    Timer = Original_Timer;

                }
            }

            if (FakeJump2 == true)
            {
                Jump3 = false;
                GetComponent<SpriteRenderer>().sprite = JumpLeft;
                Vector3 FaketargetPos2 = new Vector3(FakeTarget2.position.x, FakeTarget2.position.y + 1.2f, FakeTarget2.position.z);
                float y = FaketargetPos2.y + 10;
                Vector3 targetJumpPos = new Vector3(FakeTarget2.position.x, y, FakeTarget2.position.z);
                if (transform.position.z >= FaketargetPos2.z * 0.9)
                    transform.position = Vector3.MoveTowards(transform.position, FaketargetPos2, step);
                else
                    transform.position = Vector3.MoveTowards(transform.position, targetJumpPos, step);
            }

            if (Target3.position.x > Target2.position.x)
            {
                if (Stone3 == true && Input.GetKey(KeyCode.D))
                {
             
                    FakeStone2 = false;
                    Jump3 = true;
                    Timer = Original_Timer;
                    Arms[0].GetComponent<WaterArms>().IsActive(false);
                    Arms[1].GetComponent<WaterArms>().IsActive(false);
                    Arms[0].GetComponent<Renderer>().enabled = false;
                    Arms[1].GetComponent<Renderer>().enabled = false;
                }
            }
            if (Target3.position.x < Target2.position.x)
            {
                if (Stone3 == true && Input.GetKey(KeyCode.A))
                {
             
                    FakeStone2 = false;
                    Jump3 = true;
                    Timer = Original_Timer;
                    Arms[0].GetComponent<WaterArms>().IsActive(false);
                    Arms[1].GetComponent<WaterArms>().IsActive(false);
                    Arms[0].GetComponent<Renderer>().enabled = false;
                    Arms[1].GetComponent<Renderer>().enabled = false;
                }
            }
            if (Jump3 == true)
            {
                FakeJump2 = false;
                GetComponent<SpriteRenderer>().sprite = JumpRight;
                Vector3 targetPos3 = new Vector3(Target3.position.x, Target3.position.y + 2.7f, Target3.position.z);
                float y = targetPos3.y + 10;
                Vector3 targetJumpPos = new Vector3(Target3.position.x, y, Target3.position.z);
                if (transform.position.z >= targetPos3.z * 0.9)
                    transform.position = Vector3.MoveTowards(transform.position, targetPos3, step);
                else
                    transform.position = Vector3.MoveTowards(transform.position, targetJumpPos, step);
            }

            if (FakeTarget3.position.x > Target3.position.x)
            {
                if (FakeStone3 == true && Input.GetKey(KeyCode.D))
                {
                
                    Stone4 = false;
                    FakeJump3 = true;
                    Timer = Original_Timer;

                }
            }

            if (FakeTarget3.position.x < Target3.position.x)
            {
                if (FakeStone3 == true && Input.GetKey(KeyCode.A))
                {
            
                    Stone4 = false;
                    FakeJump3 = true;
                    Timer = Original_Timer;

                }
            }

            if (FakeJump3 == true)
            {
                Jump4 = false;
                GetComponent<SpriteRenderer>().sprite = JumpRight;
                Vector3 FaketargetPos3 = new Vector3(FakeTarget3.position.x, FakeTarget3.position.y + 2.5f, FakeTarget3.position.z);
                float y = FaketargetPos3.y + 10;
                Vector3 targetJumpPos = new Vector3(FakeTarget3.position.x, y, FakeTarget3.position.z);
                if (transform.position.z >= FaketargetPos3.z * 0.9)
                    transform.position = Vector3.MoveTowards(transform.position, FaketargetPos3, step);
                else
                    transform.position = Vector3.MoveTowards(transform.position, targetJumpPos, step);
            }
            if (Target4.position.x > Target3.position.x)
            {
                if (Stone4 == true && Input.GetKey(KeyCode.D))
                {
                
                    FakeStone3 = false;
                    Jump4 = true;
                    Timer = Original_Timer;
                    Arms[0].GetComponent<WaterArms>().IsActive(false);
                    Arms[1].GetComponent<WaterArms>().IsActive(false);
                    Arms[0].GetComponent<Renderer>().enabled = false;
                    Arms[1].GetComponent<Renderer>().enabled = false;
                }
            }

            if (Target4.position.x < Target3.position.x)
            {
                if (Stone4 == true && Input.GetKey(KeyCode.A))
                {
             
                    FakeStone3 = false;
                    Jump4 = true;
                    Timer = Original_Timer;
                    Arms[0].GetComponent<WaterArms>().IsActive(false);
                    Arms[1].GetComponent<WaterArms>().IsActive(false);
                    Arms[0].GetComponent<Renderer>().enabled = false;
                    Arms[1].GetComponent<Renderer>().enabled = false;
                }
            }
            if (Jump4 == true)
            {
                FakeJump3 = false;
                GetComponent<SpriteRenderer>().sprite = JumpLeft;
                Vector3 targetPos4 = new Vector3(Target4.position.x, Target4.position.y + 2.5f, Target4.position.z);
                float y = targetPos4.y + 10;
                Vector3 targetJumpPos = new Vector3(Target4.position.x, y, Target4.position.z);
                if (transform.position.z >= targetPos4.z * 0.9)
                    transform.position = Vector3.MoveTowards(transform.position, targetPos4, step);
                else
                    transform.position = Vector3.MoveTowards(transform.position, targetJumpPos, step);
            }

            if (Target5.position.x > Target4.position.x)
            {
                if (Stone5 == true && Input.GetKey(KeyCode.D))
                {
          
                    Jump5 = true;
                    Timer = Original_Timer;
                    Arms[0].GetComponent<WaterArms>().IsActive(false);
                    Arms[1].GetComponent<WaterArms>().IsActive(false);
                    Arms[0].GetComponent<Renderer>().enabled = false;
                    Arms[1].GetComponent<Renderer>().enabled = false;
                }
            }

            if (Target5.position.x < Target4.position.x)
            {
                if (Stone5 == true && Input.GetKey(KeyCode.A))
                {
          
                    Jump5 = true;
                    Timer = Original_Timer;
                    Arms[0].GetComponent<WaterArms>().IsActive(false);
                    Arms[1].GetComponent<WaterArms>().IsActive(false);
                    Arms[0].GetComponent<Renderer>().enabled = false;
                    Arms[1].GetComponent<Renderer>().enabled = false;
                }
            }

            if (Jump5 == true)
            {
                Debugcontrolls = false;
                GetComponent<SpriteRenderer>().sprite = JumpRight;
                Vector3 targetPos5 = new Vector3(Target5.position.x + 3, Target5.position.y, Target5.position.z);
                float y = targetPos5.y + 10;
                Vector3 targetJumpPos = new Vector3(Target5.position.x, y, Target5.position.z);
                if (transform.position.z >= targetPos5.z * 0.96)
                    transform.position = Vector3.MoveTowards(transform.position, targetPos5, step);
                else
                    transform.position = Vector3.MoveTowards(transform.position, targetJumpPos, step);
            }

            if (Finish.position.x > Target5.position.x)
            {
                if (Goal == true && Input.GetKey(KeyCode.D))
                {
                    if (!Audio.isPlaying)
      
                    Jump6 = true;
                    Timer = Original_Timer;
                    Arms[0].GetComponent<WaterArms>().IsActive(false);
                    Arms[1].GetComponent<WaterArms>().IsActive(false);
                    Arms[0].GetComponent<Renderer>().enabled = false;
                    Arms[1].GetComponent<Renderer>().enabled = false;
                }
            }

            if (Finish.position.x < Target5.position.x)
            {
                if (Goal == true && Input.GetKey(KeyCode.A))
                {
                    if(!Audio.isPlaying)
        
                    Jump6 = true;
                    Timer = Original_Timer;
                    Arms[0].GetComponent<WaterArms>().IsActive(false);
                    Arms[1].GetComponent<WaterArms>().IsActive(false);
                    Arms[0].GetComponent<Renderer>().enabled = false;
                    Arms[1].GetComponent<Renderer>().enabled = false;
                }
            }
            if (Jump6 == true)
            {
                Debugcontrolls = false;
                GetComponent<SpriteRenderer>().sprite = JumpLeft;
                Vector3 targetFinish = new Vector3(Finish.position.x, Finish.position.y - 2, Finish.position.z);
                float y = targetFinish.y + 10;
                Vector3 targetJumpPos = new Vector3(Finish.position.x, y, Finish.position.z);
                if (transform.position.z >= targetFinish.z * 0.9)
                    transform.position = Vector3.MoveTowards(transform.position, targetFinish, step);
                else
                    transform.position = Vector3.MoveTowards(transform.position, targetJumpPos, step);
            }
        }

    }
    void OnTriggerEnter(Collider col)
    {

        if (Arms[0].GetComponent<Animator>().isInitialized == true)
        {
          Audio.Play();
            Debug.Log("ScaryHands");
        }
        if (Arms[0].GetComponent<Animator>().isInitialized == false)
        {
           Audio.Stop();
            Debug.Log("ScaryStop");
        }

        if (col.gameObject.name == "EventStart")
        {

            Stone = true;
            Jumpable = false;

        }
        if (col.gameObject.name == "Stone")
        {
            Stone1 = true;
            FakeStone = true;
            Stone = false;
            Jump = false;
            TimerStart = true;
            Jumpable = true;

        }
        if (col.gameObject.name == "StoneTwo")
        {
            Stone2 = true;
            FakeStone1 = true;
            Stone1 = false;
            FakeStone = false;
            Jump1 = false;
            Jumpable = false;
            PlayerOne.GetComponent<JumpScript>().SetJumpable(true);
            Bubbles.GetComponent<Bubbles>().SetMovable(true);

        }
        if (col.gameObject.name == "StoneThree")
        {
            Stone3 = true;
            Stone2 = false;
            FakeStone1 = false;
            FakeStone2 = true;
            Jump2 = false;
            Jumpable = true;
            Bubbles.GetComponent<Bubbles>().SetMovable(true);

        }
        if (col.gameObject.name == "StoneFour")
        {
            Stone4 = true;
            Stone3 = false;
            FakeStone2 = false;
            FakeStone3 = true;
            Jump3 = false;
            Jumpable = false;
            PlayerOne.GetComponent<JumpScript>().SetJumpable(true);
            Bubbles.GetComponent<Bubbles>().SetMovable(true);

        }
        if (col.gameObject.name == "StoneFive")
        {
            Stone5 = true;
            Stone4 = false;
            FakeStone3 = false;
            Jump4 = false;
            Jumpable = true;
            Debugcontrolls = true;
            Bubbles.GetComponent<Bubbles>().SetMovable(true);

        }

        if (col.gameObject.name == "StoneSix")
        {
            Stone5 = false;
            Jump5 = false;
            Goal = true;
            Jumpable = false;
            PlayerOne.GetComponent<JumpScript>().SetJumpable(true);
            Bubbles.GetComponent<Bubbles>().SetMovable(true);

        }
        if (col.gameObject.name == "EventFinish")
        {

            Goal = false;
            Jump6 = false;
            SetEventDone(true);
            TimerStart = false;


        }
        if (col.gameObject.name == "StoneFake1")
        {
            SceneManager.LoadScene("CrossingStreamEvent");
        }
        if (col.gameObject.name == "StoneFake2")
        {
            SceneManager.LoadScene("CrossingStreamEvent");
        }
        if (col.gameObject.name == "StoneFake3")
        {
            SceneManager.LoadScene("CrossingStreamEvent");
        }
        if (col.gameObject.name == "StoneFake4")
        {
            SceneManager.LoadScene("CrossingStreamEvent");
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.name == "EventStart" || col.gameObject.name == "Stone" || col.gameObject.name == "StoneTwo" || col.gameObject.name == "StoneThree" || col.gameObject.name == "StoneFour" || col.gameObject.name == "StoneFive" || col.gameObject.name == "StoneSix")
        {
            playJumpSound = true;
        }
    }

    void SetMovable(bool p_bValue)
    {
        Moveable = p_bValue;
    }
    void SetEventDone(bool p_bValue)
    {
        EventFinished = p_bValue;
    }
    public bool EventCompleted()
    {
        return EventFinished;
    }

    public void SetJumpable(bool p_bValue)
    {
        Jumpable = p_bValue;
    }


    void PlayJumpSound()
    {
        if (playJumpSound)
        {
           if (!Audio.isPlaying)
            Audio.PlayOneShot(jumpGirl, 1);

            playJumpSound = false;
        }
    }

}
