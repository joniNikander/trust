﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class StickCollision : MonoBehaviour {
    public float Noise_Max = 100;
    public float Noise_Min = 0;
    public int Noise_Increase = 10;
    public float Timer;
    public float Noise_Decrease;
    public AudioClip TrollWake;
    public AudioClip Twig;
    public AudioClip Twig2;
    public AudioClip Twig3;
    AudioSource Audio;

    private Camera mainCam;
    private GameObject Player;
    public GameObject Light;
    public GameObject Enrage;
    public GameObject Exclamation;

    private bool playTrollSound = false;


    private float Original_Timer;
    private float Original_Noise;
    public float Restart_Timer;


    public Animator Waking;
    // Use this for initialization
    void Start () {


        Exclamation.GetComponent<SpriteRenderer>().enabled = false;
        Enrage.GetComponent<Light>().enabled = false;
        Original_Timer = Timer;
        Player = this.gameObject;
        Original_Noise = Noise_Min;
        mainCam = Camera.main;
        Waking = Waking.GetComponent<Animator>();
        Audio = this.gameObject.GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
	if(Player.GetComponent<movement_player_one>().IsPlayerIdle() == true)
        {
            Timer -= Time.deltaTime;

            if(Timer <= 0)
            {
                Noise_Min -= Noise_Decrease;
            }
        }
    if(Noise_Min <= 0)
        {
            Noise_Min = Original_Noise;
        }

        if (Noise_Min >= Noise_Max)
        {
            Restart_Timer -= Time.deltaTime;

            Enrage.GetComponent<Light>().enabled = true;

            if (Restart_Timer <= 0)
            {
                SceneManager.LoadScene("Sleeping-troll");
            }

        }
        if (Noise_Min == 35)
        {
            
           
            Light.GetComponent<Light>().intensity = 8;
            Light.GetComponent<Light>().range = 8;

        }
        else if (Noise_Min >= 70)
        {
          
            Light.GetComponent<Light>().intensity = 8;
            Light.GetComponent<Light>().range = 20;

        }

        else if (Noise_Min == 70)
        {
            Audio.PlayOneShot(TrollWake, 1);
        }

        else
        {
            Waking.SetBool("WakingUp", false);
        }

        if (Player.GetComponent<movement_player_one>().IsPlayerIdle() == false)
            Timer = Original_Timer;

        if(playTrollSound)
        {
            TrollWakingUp();
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.tag == "Sticks")
        {

            Noise_Min += Noise_Increase;
            Exclamation.GetComponent<SpriteRenderer>().enabled = true;
            Waking.SetBool("WakingUp", true);
            Waking.Play("Waking", -1, 0f);

            if (Waking.GetBool("WakingUp"))
                playTrollSound = true;

            if (Noise_Min <= 35)
            {
                Audio.PlayOneShot(Twig, 1);
            }

            else if (Noise_Min <= 70)
            {
                Audio.PlayOneShot(Twig2, 1);
            }

            else
            {
                Audio.PlayOneShot(Twig3, 1);
            }

            mainCam.GetComponent<CameraShakes>().SetShake(true);

        }
      
        if(col.gameObject.tag == "Svamp")
        {

        }

    }

    void OnTriggerExit(Collider col)
    {
        if(col.gameObject.tag == "Sticks")
        {
            Exclamation.GetComponent<SpriteRenderer>().enabled = false;
        }
    }

    void TrollWakingUp()
    {
        if (Waking.GetBool("WakingUp"))
        {
            Debug.Log("waakiee");
            if(playTrollSound)
            {
                Audio.PlayOneShot(TrollWake, 1);
                playTrollSound = false;
                
            }


        }
    }
}
