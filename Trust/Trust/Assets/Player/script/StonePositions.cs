﻿using UnityEngine;
using System.Collections;

public class StonePositions : MonoBehaviour {

    private float Puzzle;
    public Vector2 randomValues;
    public Transform OppositeStone;
    public Transform PreviousStone;
    private GameObject TargetStone;

	// Use this for initialization
	void Start () {

        Puzzle = Random.Range(randomValues.x, randomValues.y);

        TargetStone = this.gameObject;

        TargetStone.transform.position = new Vector3(TargetStone.transform.position.x + Puzzle, TargetStone.transform.position.y, TargetStone.transform.position.z);

        if (TargetStone.transform.position.x >= PreviousStone.position.x)
        {
            OppositeStone.position = new Vector3(OppositeStone.position.x - 5, OppositeStone.position.y, OppositeStone.position.z);
        }
        if (TargetStone.transform.position.x < PreviousStone.position.x)
        {
            OppositeStone.position = new Vector3(OppositeStone.position.x + 5, OppositeStone.position.y, OppositeStone.position.z);
        }

    }
	
	// Update is called once per frame
	void Update () {




    }
}
