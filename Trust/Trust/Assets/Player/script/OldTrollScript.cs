﻿using UnityEngine;
using System.Collections;

public class OldTrollScript : MonoBehaviour {

    public bool Moveable;
    public Transform Target;
    public Transform Target1;
    public Transform Target2;
    public Transform Target3;
    public Transform Target4;
    public Transform Target5;
    public Transform Finish;
    public Transform Way;


    private bool Stone;
    private bool Stone1;
    private bool Stone2;
    private bool Stone3;
    private bool Stone4;
    private bool Stone5;
    private bool Stone6;
    private bool Goal;
    public bool Path;

    public float Speed;
    public float Timer;
    private float stoneWaitTimer;
    private int Puzzle;

    private GameObject TargetStone;
    private GameObject Faerie;
    private Animator Elf;

    private Renderer Elve;
    private Color newcolor;

    private Animator Blob;

    // Use this for initialization
    void Start () {

        Elve = GetComponent<Renderer>();

        newcolor = Elve.material.color;

        Faerie = GameObject.FindGameObjectWithTag("Faerie");

        Elf = this.gameObject.GetComponent<Animator>();

        Elf.enabled = false;
        Moveable = false;
        Stone = true;
        Stone1 = false;
        Stone2 = false;
        Stone3 = false;
        Stone4 = false;
        Stone5 = false;
        Path = false;

        stoneWaitTimer = 1.0f;       

    }
	
	// Update is called once per frame
	void Update () {
        float step = Speed * Time.deltaTime;

        Timer -= Time.deltaTime;

        if(Timer <= 0 && Faerie.GetComponent<StreamFaerie>().Moveable == false)
        {
            Elf.enabled = true;
            Moveable = true;
            

        }
        if (Moveable == true)
        {
            if (Stone == true )
            {
                Vector3 targetPos = new Vector3(Target.position.x, Target.position.y + 2, Target.position.z);
                float y = targetPos.y + 10;
                Vector3 targetJumpPos = new Vector3(Target.position.x, y, Target.position.z);
                if (transform.position.z >= targetPos.z * 0.8)
                    transform.position = Vector3.MoveTowards(transform.position, targetPos, step);
                else
                    transform.position = Vector3.MoveTowards(transform.position, targetJumpPos, step);
            }
            if (Stone1 == true && Stone == false)
            {
                Vector3 targetPos1 = new Vector3(Target1.position.x, Target1.position.y + 2f, Target1.position.z);
                float y = targetPos1.y + 10;
                Vector3 targetJumpPos = new Vector3(Target1.position.x, y, Target1.position.z);
                if (transform.position.z >= targetPos1.z * 0.9)
                    transform.position = Vector3.MoveTowards(transform.position, targetPos1, step);
                else
                    transform.position = Vector3.MoveTowards(transform.position, targetJumpPos, step);
            }
            if (Stone2 == true && Stone1 == false)
            {
                Vector3 targetPos2 = new Vector3(Target2.position.x, Target2.position.y + 2f, Target2.position.z);
                float y = targetPos2.y + 10;
                Vector3 targetJumpPos = new Vector3(Target2.position.x, y, Target2.position.z);
                if (transform.position.z >= targetPos2.z * 0.9)
                    transform.position = Vector3.MoveTowards(transform.position, targetPos2, step);
                else
                    transform.position = Vector3.MoveTowards(transform.position, targetJumpPos, step);
            }
            if (Stone3 == true && Stone2 == false)
            {
                Vector3 targetPos3 = new Vector3(Target3.position.x, Target3.position.y + 2f, Target3.position.z);
                float y = targetPos3.y + 10;
                Vector3 targetJumpPos = new Vector3(Target3.position.x, y, Target3.position.z);
                if (transform.position.z >= targetPos3.z * 0.9)
                    transform.position = Vector3.MoveTowards(transform.position, targetPos3, step);
                else
                    transform.position = Vector3.MoveTowards(transform.position, targetJumpPos, step);
            }
            if (Stone4 == true && Stone3 == false)
            {
                Vector3 targetPos4 = new Vector3(Target4.position.x, Target4.position.y + 2f, Target4.position.z);
                float y = targetPos4.y + 10;
                Vector3 targetJumpPos = new Vector3(Target4.position.x, y, Target4.position.z);
                if (transform.position.z >= targetPos4.z * 0.9)
                    transform.position = Vector3.MoveTowards(transform.position, targetPos4, step);
                else
                    transform.position = Vector3.MoveTowards(transform.position, targetJumpPos, step);
            }
            if (Stone5 == true && Stone4 == false)
            {
                Vector3 targetPos5 = new Vector3(Target5.position.x + 4, Target5.position.y + 2f, Target5.position.z);
                float y = targetPos5.y + 10;
                Vector3 targetJumpPos = new Vector3(Target5.position.x, y, Target5.position.z);
                if (transform.position.z >= targetPos5.z * 0.95)
                    transform.position = Vector3.MoveTowards(transform.position, targetPos5, step);
                else
                    transform.position = Vector3.MoveTowards(transform.position, targetJumpPos, step);
            }
            if (Goal == true && Stone5 == false)
            {
                Vector3 targetPos6 = new Vector3(Finish.position.x, Finish.position.y + 2f, Finish.position.z);
                float y = targetPos6.y + 10;
                Vector3 targetJumpPos = new Vector3(Finish.position.x, y, Finish.position.z);
                if (transform.position.z >= targetPos6.z * 0.9)
                    transform.position = Vector3.MoveTowards(transform.position, targetPos6, step);
                else
                    transform.position = Vector3.MoveTowards(transform.position, targetJumpPos, step);
            }
            if (Goal == false && Path == true)
            {
                transform.position = Vector3.MoveTowards(transform.position, Way.position, step);
            }
        }




    }
    void OnTriggerEnter(Collider col)
    {

        if (col.gameObject.name == "Stone")
        {
                Stone1 = true;
                Stone = false;
                Elf.speed = 0;
                Moveable = false;
                Faerie.GetComponent<StreamFaerie>().SetMovable(true);





        }
        if (col.gameObject.name == "StoneTwo")
        {
            Stone2 = true;
            Stone1 = false;
        }
        if (col.gameObject.name == "StoneThree")
        {
            Stone3 = true;
            Stone2 = false;
            Moveable = false;
            Faerie.GetComponent<StreamFaerie>().SetMovable(true);

        }
        if (col.gameObject.name == "StoneFour")
        {
            Stone4 = true;
            Stone3 = false;

        }
        if (col.gameObject.name == "StoneFive")
        {
            Stone5 = true;
            Stone4 = false;
            Moveable = false;
            Faerie.GetComponent<StreamFaerie>().SetMovable(true);

        }

        if (col.gameObject.name == "StoneSix")
        {
            Stone5 = false;
            Goal = true;

        }
        if (col.gameObject.name == "EventFinish")
        {
            Path = true;
            Goal = false;
            Faerie.GetComponent<StreamFaerie>().SetMovable(true);

        }
    }

    void OnTriggerStay(Collider col)
    {
        if (col.name == "PathOut")
        {
            newcolor.a -= 1f * Time.deltaTime;
            Elve.material.color = newcolor;
            if (Elve.material.color.a <= 0)
            {
                GetComponent<OldTrollScript>().enabled = false;
                Elve.enabled = false;
            }
        }

    }
    public void SetMovable(bool p_bValue)
    {
        Moveable = p_bValue;
    }

   
}
