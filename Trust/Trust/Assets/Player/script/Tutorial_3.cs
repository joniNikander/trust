﻿using UnityEngine;
using System.Collections;

public class Tutorial_3 : MonoBehaviour {
    private Animator anim;
    public GameObject target;
    public bool isDebug;

    // Use this for initialization
    void Start () {
        anim = GetComponentInParent<Animator>();
        anim.SetInteger("State", 1);
        GetComponentsInChildren<ParticleSystem>()[0].Stop();
        GetComponentsInChildren<ParticleSystem>()[1].Stop();

        GetComponentsInChildren<Light>()[0].enabled = false;
        GetComponentsInChildren<Light>()[1].enabled = false;
        GetComponentsInChildren<Light>()[2].enabled = false;

        GetComponent<Tutorial_3>().enabled = false;
    }
	
	// Update is called once per frame
	void Update () {
        if(isDebug)
        {
            if (Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.LeftArrow) && Input.GetKey(KeyCode.RightArrow))
            {
                target.GetComponent<Animator>().SetInteger("direction", 1);
                target.GetComponent<Animator>().SetInteger("tut", 0);

                target.GetComponent<movement_player_one>().enabled = true;
            }
        }
        else if (!isDebug)
        {
            if (Input.GetKey(KeyCode.Joystick1Button3) && Input.GetKey(KeyCode.Joystick1Button1) && Input.GetKey(KeyCode.Joystick2Button3) && Input.GetKey(KeyCode.Joystick2Button1))
            {
                target.GetComponent<Animator>().SetInteger("direction", 1);
                target.GetComponent<Animator>().SetInteger("tut", 0);
                GetComponent<Tutorial_3>().enabled = false;
                target.GetComponent<movement_player_one>().enabled = true;
            }
        }
    }
    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player_One")
        {
            GetComponent<Tutorial_3>().enabled = true;

            GetComponentsInChildren<ParticleSystem>()[0].Play();
            GetComponentsInChildren<ParticleSystem>()[1].Play();

            GetComponentsInChildren<Light>()[0].enabled = true;
            GetComponentsInChildren<Light>()[1].enabled = true;
            GetComponentsInChildren<Light>()[2].enabled = true;

            anim.SetInteger("State", 2);

            target.GetComponent<movement_player_one>().enabled = false;
            target.GetComponent<Animator>().SetInteger("direction", 0);
            target.GetComponent<Animator>().SetInteger("tut", 4);
            
        }
    }
    void OnTriggerExit(Collider col)
    {
        if(col.gameObject.tag == "Player_One")
        {
            anim.SetInteger("State", 1);

            GetComponentsInChildren<ParticleSystem>()[0].Stop();
            GetComponentsInChildren<ParticleSystem>()[1].Stop();

            GetComponentsInChildren<Light>()[0].enabled = false;
            GetComponentsInChildren<Light>()[1].enabled = false;
            GetComponentsInChildren<Light>()[2].enabled = false;

            target.GetComponent<Animator>().SetInteger("tut", 0);
            GetComponent<Tutorial_3>().enabled = false;
        }
    }
}
