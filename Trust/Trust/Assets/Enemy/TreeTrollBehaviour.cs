﻿using UnityEngine;
using System.Collections;

public class TreeTrollBehaviour : MonoBehaviour {
    private Animator anim;
    public GameObject target;
    public AudioClip fearSound;
    public float timer = 0.0f;
    private Light[] lights;
    private ParticleSystem[] eyes;
    private bool playSound = false;
    private bool hasHugged = false;
    float volume;
   
    public AudioClip treeTrollSound;
    AudioSource Audio;
    private bool playTreeTrollSound = false;

    // Use this for initialization
    void Start () {
        anim = this.GetComponent<Animator>();
        eyes = this.GetComponentsInChildren<ParticleSystem>();
        lights = GetComponentsInChildren<Light>();

        Audio = this.gameObject.GetComponent<AudioSource>();
        

        timer = Random.Range(0.0f, 6.0f);
        for(int i = 0; i < eyes.Length; i++)
        {
            eyes[i].Stop();
        }
        for(int i = 0; i < lights.Length; i++)
        {
            lights[i].enabled = false;
        }
        anim.enabled = false;
    }
	
	// Update is called once per frame
	void Update () {

        if (playSound)
        {
            volume = (target.GetComponent<Fear>().GetCurrentFear() - 0f) / (target.GetComponent<Fear>().FearMaximumValue - 0f);
           // GameObject.FindGameObjectWithTag("Bakground_Music").GetComponent<AudioSource>().volume = 0.3f;
           // if (!target.GetComponent<AudioSource>().isPlaying)
              //  PlayFearSound();
        }
    }

    void OnTriggerEnter(Collider col)
    {
        
        if(col.tag == "Player_One")
        {
            anim.SetInteger("State", 2);
            playTreeTrollSound = true;

            if (playTreeTrollSound == true)
            {
                PlayTreeTrollSound();

                Debug.Log("SNÄLLA");
            }
            
            anim.enabled = true;
            hasHugged = false;
            

            for (int i = 0; i < eyes.Length; i++)
            {
                eyes[i].Play();
            }

            for (int i = 0; i < lights.Length; i++)
            {
                lights[i].enabled = true;
            }
        }

    }
    void OnTriggerStay(Collider col)
    {
        if (col.tag == "Player_One")
        {
           
            if (timer >= 0f)
            {
                playSound = true;
                for (int i = 0; i < eyes.Length; i++)
                {
                    eyes[i].Play();
                }

                for (int i = 0; i < lights.Length; i++)
                {
                    lights[i].enabled = true;
                }
                if (!hasHugged)
                    target.GetComponent<Fear>().IncreaseFear();
                if (target.GetComponent<movement_player_one>().IsHugging())
                    hasHugged = true;
                timer -= Time.deltaTime;
                anim.SetInteger("State", 2);
            }
            else if (timer <= 0f)
            {
                timer = Random.Range(0f, 4f);

                anim.SetInteger("State", 3);
                for (int i = 0; i < eyes.Length; i++)
                {
                    eyes[i].Stop();
                }

                for (int i = 0; i < lights.Length; i++)
                {
                    lights[i].enabled = false;
                }
            }
        }

       
    }
    void OnTriggerExit(Collider col)
    {
        
        if (col.tag == "Player_One")
        {
            
            for (int i = 0; i < eyes.Length; i++)
            {
                eyes[i].Stop();
            }
            for (int i = 0; i < lights.Length; i++)
            {
                lights[i].enabled = false;
            }

        }
        anim.SetBool("TurnBack", true);
       
    }
    private void PlayFearSound()
    {
        GetComponent<AudioSource>().PlayOneShot(fearSound, volume);
        playSound = false;
    }

    void PlayTreeTrollSound()
    {
        Audio.PlayOneShot(treeTrollSound, 1);
        playTreeTrollSound = false;
        Debug.Log("FAKU");
    }
}
