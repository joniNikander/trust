﻿using UnityEngine;
using System.Collections;

public class parallax : MonoBehaviour {
    private GameObject target;

    public float Parallax_Speed_x;
    public float Parallax_Speed_y;
    public float Parallax_Speed_z;
    public bool Parallax_Reverse_Direction_x;
    public bool move_in_x_direction;
    // Use this for initialization
    void Start () {
        target = GameObject.FindGameObjectWithTag("Player");
    }
	
	// Update is called once per frame
	void Update () {

        if(target.GetComponent<movement_player_one>().IsMovingDirection() == 1)
        transform.Translate(new Vector3(Parallax_Speed_x, 0.0f, 0.0f));

        if (target.GetComponent<movement_player_one>().IsMovingDirection() == 2)
            transform.Translate(new Vector3(-Parallax_Speed_x, 0.0f, 0.0f));

        if (target.GetComponent<movement_player_one>().IsMovingDirection() == 3)
        {
            if(move_in_x_direction)
            {
                if (Parallax_Reverse_Direction_x)
                {
                    transform.Translate(new Vector3(-Parallax_Speed_x, Parallax_Speed_y, -Parallax_Speed_z));
                }
                else
                    transform.Translate(new Vector3(Parallax_Speed_x, Parallax_Speed_y, -Parallax_Speed_z));
            }
            else
                transform.Translate(new Vector3(0.0f, Parallax_Speed_y, -Parallax_Speed_z));
        }
	}
}
